"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from coeur.views import *
from django.conf.urls.static import static
from django.conf import settings, urls
from django.views.static import serve


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', StaticView.as_view(), name='welcome_page'),
    path('presentation/', StaticView.as_view(template_name="presentation.html"), name='presentation'),
    path('feat/output/choice/', OutputFeatureView.as_view(), name='feature_output_choice'),
    path('waitlist/', Waitlist.as_view(), name='waitlist'),
    path('waitlist/<str:date>/', Waitlist.as_view(), name='waitlist'),
    path('patient/', PatientView.as_view(), name='patient'),
    path('patient/<str:id_patient>/', PatientView.as_view(), name='patient'),
    path('decision/explanation/', DecisionExplanationView.as_view(), name='decision_explanation'),
    path('decision/explanation/<str:id_decision>/', DecisionExplanationView.as_view(), name='decision_explanation'),
    path('rankings/<int:id_donneur>/', Rankings.as_view(), name='rankings'),
    path('rankings/', Rankings.as_view(), name='rankings'),
    path('decision/tree/', GlobalExplanation.as_view(), name='decision_tree'),
    path('partial/dependence/plot/', PartialDepExplView.as_view(), name='partial_dep'),
    urls.re_path(r'^static/(?P<path>.*)$', serve,{'document_root': settings.STATIC_ROOT}),
]
