import operator

from sklearn.tree._export import _MPLTreeExporter
from matplotlib.text import Annotation
from sklearn.tree._reingold_tilford import buchheim, Tree
from sklearn import tree
from sklearn.tree import _tree
import networkx as nx
import sklearn.tree as tree
import pylab as plt
import numpy as np
from io import StringIO
from bokeh.models import Plot, Range1d, Rect, HoverTool, TapTool, BoxSelectTool, SaveTool
from bokeh.models.graphs import from_networkx
from bokeh.models.annotations import LabelSet, Label
from bokeh.palettes import Magma256
from scipy.sparse import csr_matrix

from mysite.settings import *
from coeur.utils import *

def pixel_width(label):
    """
    Width in pixel form rectangle in bokeh decision tree
    :param label:
    :return:
    """
    size = max(map(len, label.split('\n')))
    return size * 3.1


def pixel_height(label):
    """
    Height in pixel form rectangle in bokeh decision tree
    :param label:
    :return:
    """
    return (len([line for line in label.split('\n') if line != '']) + 1) * 10


def set_pos_dict(g, parent, node, pos_dict, dx=1, dy=10, root_coord=(0, 1), eps=0.5):
    if parent is None:
        node = get_root(g)
        x, y = root_coord
    else:
        x, y = pos_dict[parent]
        y = y - dy
        edge = g.get_edge_data(parent, node)
        if edge['name'] == 'yes':
            x = x + dx
        else:
            x = x - dx
    pos_dict[node] = np.array((x, y))

    children = [dest for orig, dest in g.edges if orig == node]
    for child in children:
        set_pos_dict(g, node, child, pos_dict, dx=dx * eps)


def identity(arg):
    return arg


def get_root(g):
    root = [node for node, deg in g.degree() if deg == 2]
    if len(root) != 1:
        raise Exception('something wrong')
    else:
        return root[0]


def rbm_format_rule(predicate):
    if '__is__' in predicate:
        feat, val = predicate.split('__is__')
        if ' > ' in predicate:
            op = ' est '
        elif ' <= ' in predicate:
            op = " n'est pas "
        else:
            raise ValueError("rule ", predicate, " not well formated")
        val = val.replace(' > 0.5', '').replace(' <= 0.5', '')
        formated_pred = VAR_NAME_DICT[feat] + op + val
    else:
        for op in [' <= ', ' > ', ' == ']:
            if len(predicate.split(op)) > 1:
                feat, val = predicate.split(op)
                formated_pred = VAR_NAME_DICT[feat] + op + str(round(float(val), 2))
    return formated_pred


def tree_to_code(tree, feature_names):
    """
    Outputs a decision tree model as a Python function

    Parameters:
    -----------
    tree: decision tree model
        The decision tree to represent as a function
    feature_names: list
        The feature names of the dataset used for building the decision tree
    """

    tree_ = tree.tree_
    feature_name = [
        feature_names[i] if i != _tree.TREE_UNDEFINED else "undefined!"
        for i in tree_.feature
    ]
    g = nx.DiGraph()

    def recurse(node, depth, g):
        if tree_.feature[node] != _tree.TREE_UNDEFINED:
            name = feature_name[node]
            threshold = tree_.threshold[node]
            categorical = False
            if '__is__' in name:
                name, threshold = name.split('__is__')
                categorical = True
            node_info = {"type": 'parent',
                         "categorical": categorical,
                         'threshold': threshold,
                         'value': round(tree_.value[node][0][0], 1),
                         'name': name,
                         'id': node}
            node_name = repr(node_info)
            g.add_node(node_name)
            cl_name = recurse(tree_.children_left[node], depth + 1, g)
            g.add_edge(node_name, cl_name, name='yes')
            cr_name = recurse(tree_.children_right[node], depth + 1, g)
            g.add_edge(node_name, cr_name, name='no')
        else:
            node_info = {"type": 'leaf',
                         'value': round(tree_.value[node][0][0], 1),
                         'id': node}
            node_name = repr(node_info)
            g.add_node(node_name)
        return node_name

    recurse(0, 1, g)
    return g


def display_decision_tree(dt, feature_names, class_names=None, save_file=False):
    dot_data = StringIO()
    if save_file:
        tree.export_graphviz(dt,
                             out_file=dot_data,
                             feature_names=feature_names,
                             class_names=class_names,
                             filled=True, rounded=True,
                             impurity=False)

        graph = pydot.graph_from_dot_data(dot_data.getvalue())
        graph[0].write_png(save_file)
    img = plt.imread(save_file)
    plt.figure(figsize=(img.shape[0] / 40., img.shape[1] / 40.))
    plt.imshow(img)
    plt.axis('off')
    plt.show()


def display_decision_tree_bokeh(dt, data, feature_names, output_name):

    g = tree_to_code(dt, list(feature_names))
    # Using positioning from sklearn
    exporter = _MPLTreeExporter()
    my_tree = exporter._make_tree(0, dt.tree_, dt.criterion)
    draw_tree = buchheim(my_tree)

    # important to make sure we're still
    # inside the axis after drawing the box
    # this makes sense because the width of a box
    # is about the same as the distance between boxes
    ax = plt.gca()
    ax.clear()
    ax.set_axis_off()

    max_x, max_y = draw_tree.max_extents() + 1
    ax_width = ax.get_window_extent().width
    ax_height = ax.get_window_extent().height

    scale_x = ax_width / max_x
    scale_y = ax_height / max_y

    exporter.recurse(draw_tree, dt.tree_, ax, scale_x, scale_y, ax_height)

    anns = [ann for ann in ax.get_children()
            if isinstance(ann, Annotation)]

    canopy = sorted(set([ann.xy for ann in anns]), key=lambda x: (-x[1], x[0]))[0:3]
    label_true_xy = np.mean([canopy[0], canopy[1]], axis=0)
    label_false_xy = np.mean([canopy[0], canopy[2]], axis=0)

    node_info = {}
    set_pos_dict(g, None, None, node_info, dx=50, dy=3)
    values = []
    for idx, k in enumerate(node_info.keys()):
        node_info_item = eval(k)
        values.append(node_info_item['value'])
    # modify positioning and add info to each node


    for idx, k in enumerate(node_info.keys()):
        node_info_item = eval(k)
        node_info_item['id'] = int(node_info_item['id'])
        # ATTENTION ! POUR UN AFFICHAGE CORRECT DE L'ARBRE DE DÉCISION, IL EST PRÉFÉRABLE
        # D'INVERSER LES BOOLÉENS. En effet, l'affiche de sklearn présente les variables catégorielles
        # de la manière suivate : var__is__value <= 0.5 qui renvoie var==value à droite
        # (double négation). Pour les mettre dans le bon sens, il faut inverser tous les booléens...
        # ATTENTION AUX UTILISATIONS DE CETTE FONCTION HORS explicoeur
        # for col in data.columns:
        #     if '__is__' in col:
        #         data.loc[:, col] = 1 - data[col]

        leaf_data = data[np.array(csr_matrix.todense(dt.decision_path(data[feature_names])[:, int(node_info_item['id'])]).flatten())[0]==1]
        node_info_item['score_hover'] = mean_min_max_info(leaf_data['output'])
        if 'age_r' in feature_names:
            node_info_item['age_r'] = mean_min_max_info(leaf_data['age_r'])
        if 'cec__is__True' in feature_names:
            node_info_item['cec'] = mean_min_max_info(leaf_data['cec__is__True'])
        if 'probnp' in feature_names:
            node_info_item['probnp'] = mean_min_max_info(leaf_data['probnp'])
        node_info_item['n_decisions'] = len(leaf_data)

        if node_info_item['type'] == "parent":
            node_info_item['color'] = "white"
            if node_info_item['categorical']:
                # reverse binary categorical value to avoid double negation (HACK)
                if node_info_item['name'] == 'drg':
                    feature = node_info_item['name']
                    value = node_info_item['threshold']
                    if value == 'False':
                        value = 'O'
                    elif value == 'O':
                        value = 'False'
                    node_info_item['label'] = VAR_NAME_DICT[feature] + " est " + format_feature_value(feature, value)
                elif node_info_item['name'] == 'cec':
                    feature = node_info_item['name']
                    value = node_info_item['threshold']
                    if value in [False, 'False']:
                        value = 'O'
                    elif value in ['O', 'True', True]:
                        value = 'False'
                    node_info_item['label'] = VAR_NAME_DICT[feature] + " est " + format_feature_value(feature, value)
                else:
                    feature = node_info_item['name']
                    value = node_info_item['threshold']
                    node_info_item['label'] = VAR_NAME_DICT[feature] + " n'est pas " + format_feature_value(feature, value)
            else:
                node_info_item['label'] = VAR_NAME_DICT[node_info_item['name']] + " <= " + str(round(node_info_item['threshold'], 2))
            node_info_item['label'] += '\n' + str(node_info_item['value'])
        if node_info_item['type'] == "leaf":
            c_index = 155 + int((node_info_item['value'] - min(values)) / (max(values) - min(values)) * 100)
            node_info_item['color'] = Magma256[c_index]
            node_info_item['label'] =  VAR_NAME_DICT[output_name] + '\n  ' + str(node_info_item['value']) + '  \n'
        node_info_item['position'] = np.array(anns[idx].xyann)
        node_info[k] = node_info_item

    G = g
    pos_dict = {k: v['position'] for k, v in node_info.items()}
    # horizontal dilatation
    pos_dict = {k: (v[0] * 1.2, v[1]) for k, v in pos_dict.items()}

    x_max, y_max = np.max(list(pos_dict.values()), axis=0)
    x_min, y_min = np.min(list(pos_dict.values()), axis=0)
    x_range = Range1d(x_min - 50, x_max + 50)
    y_range = Range1d(y_min - 50, y_max + 50)

    plot = Plot(plot_width=1000, plot_height=600, x_range=x_range, y_range=y_range)
    plot.title.text = "Graph Interaction Demonstration"
    tootips = [
        (VAR_NAME_DICT[output_name], "@value"),
        ("#Decisions", '@n_decisions'),
    ]
    if 'age_r' in feature_names:
        tootips.append(("Âge receveur", '@age_r'))
    if "cec__is__True" in feature_names:
        tootips.append(("CEC", '@cec'))
    if 'probnp' in feature_names:
        tootips.append(("Probnp", '@probnp'))

    hover = HoverTool(tooltips=tootips)
    plot.add_tools(hover, BoxSelectTool())
    plot.add_tools(hover, SaveTool())

    gr = from_networkx(G, pos_dict, scale=1, center=(0, 0))

    gr.node_renderer.data_source.data['value'] = [v['score_hover'] for v in node_info.values()]
    if 'age_r' in feature_names:
        gr.node_renderer.data_source.data['age_r'] = [v['age_r'] for v in node_info.values()]
    if "cec__is__True" in feature_names:
        gr.node_renderer.data_source.data['cec'] = [v['cec'] for v in node_info.values()]
    if 'probnp' in feature_names:
        gr.node_renderer.data_source.data['probnp'] = [v['probnp'] for v in node_info.values()]
    gr.node_renderer.data_source.data['n_decisions'] = [v['n_decisions'] for v in node_info.values()]
    gr.node_renderer.data_source.data['label_l1'] = [v['label'].split('\n')[0] for v in node_info.values()]
    gr.node_renderer.data_source.data['label_l2'] = [v['label'].split('\n')[1] for v in node_info.values()]
    gr.node_renderer.data_source.data['width'] = [pixel_width(v['label']) for v in node_info.values()]
    gr.node_renderer.data_source.data['height'] = [pixel_height(v['label']) for v in node_info.values()]
    gr.node_renderer.data_source.data['x'] = [v[0] for v in pos_dict.values()]
    gr.node_renderer.data_source.data['y'] = [v[1] for v in pos_dict.values()]
    y = gr.node_renderer.data_source.data['y']
    label_l2 = gr.node_renderer.data_source.data['label_l2']
    gr.node_renderer.data_source.data['y'] = [y + 5 if l != '' else y for y, l in zip(y, label_l2) ]
    gr.node_renderer.data_source.data['y_l2'] = [v[1] - 5 for v in pos_dict.values()]
    gr.node_renderer.data_source.data['color'] = [v['color'] for v in node_info.values()]
    gr.node_renderer.data_source.data['size'] = list(range(len(G)))

    gr.node_renderer.glyph = Rect(width='width',
                                  height='height',
                                  fill_color='color',
                                  fill_alpha=1)
    label_true = Label(x=label_true_xy[0] * 1.2 * .9, y=label_true_xy[1],
                       text='Vrai')
    label_false = Label(x=label_false_xy[0] * 1.2, y=label_false_xy[1],
                       text='Faux')

    labels_l1 = LabelSet(x='x', y='y', text='label_l1',
                         source=gr.node_renderer.data_source,
                         text_align='center', text_baseline='middle',
                         text_font_size="8px")
    labels_l2 = LabelSet(x='x', y='y_l2', text='label_l2',
                         source=gr.node_renderer.data_source,
                         text_align='center', text_baseline='middle',
                         text_font_size="8px")

    plot.renderers.append(gr)
    plot.renderers.append(labels_l1)
    plot.renderers.append(labels_l2)
    plot.renderers.append(label_true)
    plot.renderers.append(label_false)

    return plot

def display_rules(rule):
    message = ""
    message += "RULE:\n"
    message += "Local model : If:\n\t" + '\n\tand '.join(rule[0].split(' and ')) + "\nthen the output of the model equals the output of the scope with "
    message += "a precision of " + str(rule[1]["precision"])
    print(message)


# def display_rule_2d(rules, n_pixels=cfg['base']['image']['n_pixel']):
#     rule_colors = ['red', 'white', 'blue']
#     legends = ['original value', 'no rule', 'modified']
#     for r in rules:
#         predicate_list = r[0].split(' and ')
#         modified_index = [re.split('>|>=', pred)[0] for pred in predicate_list if ('>' in pred) or ('>=' in pred)]
#         unmodified_index = [re.split('<|<=', pred)[0] for pred in predicate_list if ('<=' in pred) or ('<' in pred)]
#
#         data = np.zeros(n_pixels**2)
#
#         for modif_index in modified_index:
#             if modified_index:
#                 data[int(modif_index)] = 1
#         for unmodif_index in unmodified_index:
#             if unmodified_index:
#                 data[int(unmodif_index)] = -1
#
#         data = data.reshape((n_pixels, n_pixels))
#
#         # create discrete colormap
#         cmap = colors.ListedColormap(rule_colors)
#         bounds = [-1, -.5, +.5, 1]
#         norm = colors.BoundaryNorm(bounds, cmap.N)
#
#         fig, ax = plt.subplots()
#         ax.imshow(data, cmap=cmap, norm=norm)
#
#         patches = [
#             mpatches.Patch(color=rule_colors[i], label=legends[i]) for i in range(len([-1, 0, 1]))
#                    ]
#         # put those patched as legend-handles into the legend
#         plt.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
#
#         plt.show()
