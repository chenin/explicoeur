import copy
import numpy as np
import operator
from scipy.stats import ttest_ind

from justify.justification import *
from justify.contestation import *

def check_predicate(predicate, sample):
    assert len(sample) == 1
    feature, value, operator = predicate
    return len(sample.loc[operator(sample[feature], value)]) == 1


def find_best_predicate(samples, predicate_cand, negative):
    for it, (feat, val, op) in enumerate(predicate_cand):
        # select corresponding subset
        subset = samples.loc[op(samples[feat], val), 'expected']
        mean, size = subset.mean(), len(subset)
        if 0 < size and size < len(samples):
            t_val, pvalue = ttest_ind(samples['expected'], subset, equal_var=False)
            try:
                if negative:
                    if t_val < best_cand[6]:
                        best_cand = (feat, val, op, size, mean, pvalue, t_val)
                elif t_val > best_cand[6]:
                    best_cand = (feat, val, op, size, mean, pvalue, t_val)
            except UnboundLocalError:
                best_cand = (feat, val, op, size, mean, pvalue, t_val)
    return best_cand


def all_possible_splits(samples, n_quantiles_numeric=10):
    categorical_features = [x for x in samples.columns if samples[x].dtype == 'object']
    other_features = [x for x in samples.columns if samples[x].dtype != 'object']
    splits = []
    for cat_feat in categorical_features:
        for val in samples[cat_feat].unique():
            splits.append((cat_feat, val, operator.eq))

    quantiles = np.linspace(0, 1, n_quantiles_numeric + 1)[1:-1]
    all_quantiles = samples[other_features].quantile(q=quantiles, numeric_only=False)
    for col in all_quantiles.iteritems():
        feat = col[0]
        threshold_values_serie = col[1]
        for t in threshold_values_serie.iteritems():
            splits.append((feat, t[1], operator.ge))
            splits.append((feat, t[1], operator.le))
    return splits


def operator_to_symbol(op):
    if op == operator.eq:
        return " est "
    elif op == operator.lt:
        return " < "
    elif op == operator.gt:
        return " > "
    elif op == operator.le:
        return " <= "
    elif op == operator.ge:
        return " >= "
    elif op == operator.ne:
        return " n'est pas "
    else:
        raise ValueError("Operator not valid.")


def format_rules(rules, contestation, norm, data, contest_df):
    all_rules = contestation.predicate_list
    for pred in rules:
        all_rules.append((pred[0], pred[2], str(pred[1])))
    contest = ContestVal(all_rules, contestation.target_predicate)
    ttest, pval = norm.ttest(contest, data, contest_df)
    rule_tuple = [(pred[0], operator_to_symbol(pred[1]), str(pred[2])) for pred in all_rules]
    return rule_tuple, "", ttest


def pro_and_cons_rules(contest_sample, contestation, norm, data, features=None):
    samples = justification_dataset(contestation, data, 'undetermined')
    if contestation.type == "values":
        samples.loc[:, 'expected'] = norm._eval(samples)
        del samples['y_true']
    elif contestation.type == "relative":
        c_cols, r_cols = sort_plaintiff_relative_cols(samples)
        samples.loc[:, 'expected'] = norm._eval(samples[c_cols])
        # del samples['y_pred_c'], samples['y_pred_r']
        del samples['y_true_c'], samples['y_true_r']
        # c_cols.remove('y_pred_c')
        c_cols.remove('y_true_c')
        samples = paired_to_unique_dataset(samples)
        samples = samples[[col for col in samples.columns if col[-2:]=='_c' or col=='expected']]
        samples.rename(columns={col: (col[:-2] if col!='expected' else 'expected') for col in samples.columns}, inplace=True)
        # UNCOMMENT FOR FEATURES THAT ARE COMPARISON OF REFERENCE AND PLAINTIFF
        # categorical_features = [x for x in samples.columns if samples[x].dtype == 'object']
        # other_features = [x for x in samples.columns if samples[x].dtype != 'object']
        # for col in c_cols:
        #     if col in categorical_features:
        #         if col != 'expected':
        #             samples[col[:-2] + '__IS'] = (samples[col] == samples[col[:-2] + '_r'])
        #             contest_sample[col[:-2] + '__IS'] = (contest_sample[col] == contest_sample[col[:-2] + '_r'])
        #     if col in other_features:
        #         if col != 'expected':
        #             samples[col[:-2] + '__LT'] = (samples[col] < samples[col[:-2] + '_r'])
        #             contest_sample[col[:-2] + '__LT'] = (contest_sample[col] < contest_sample[col[:-2] + '_r'])
        #     del samples[col], samples[col[:-2] + '_r']
        #     del contest_sample[col], contest_sample[col[:-2] + '_r']
    if all(samples['expected'] == 1):  # all samples are in the correct class
        return None, None   # return empty model
    elif all(samples['expected'] == 0):  # all samples are in the incorrect class
        return None, None   # return empty model
    predicate_cand = all_possible_splits(samples[features])
    predicate_cand = [pred for pred in predicate_cand if check_predicate(pred, contest_sample)]
    rbm_challenger = []
    samples_challenger = samples.copy()
    while len(rbm_challenger) <=3:
        # print("target: ", target_mean_challenger, "current", current_mean)
        best_predicate_challenger = find_best_predicate(samples_challenger, predicate_cand, negative=True)
        if best_predicate_challenger[5] > 10**-4:
            # print("No model just found", best_predicate_challenger)
            if len(rbm_challenger)==0:
                rbm_challenger = None
            break
        else:
            # print("Adding a rule", best_predicate_challenger)
            rbm_challenger.append(best_predicate_challenger)
            feature, value, operator = best_predicate_challenger[0:3]
            samples_challenger = samples_challenger.loc[operator(samples_challenger[feature], value)]

    # model side RBJ
    rbm_model = []
    samples_model = samples.copy()
    while len(rbm_model) <=3:
        # print("target: ", target_mean_model, "current", current_mean)
        best_predicate_model = find_best_predicate(samples_model, predicate_cand, negative=False)
        if best_predicate_model[5] > 10**-4:
            # print("No model just found", best_predicate_model)
            if len(rbm_model)==0:
                rbm_model = None
            break
        else:
            # print("Adding a rule", best_predicate_model)
            rbm_model.append(best_predicate_model)
            feature, value, operator = best_predicate_model[0:3]
            samples_model = samples_model.loc[operator(samples_model[feature], value)]

    if rbm_challenger is not None and rbm_model is not None:
        return format_rules(rbm_challenger, contestation, norm, data, samples_challenger), format_rules(rbm_model, contestation, norm, data, samples_model)
    elif rbm_challenger is not None:
        return format_rules(rbm_challenger, contestation, norm, data, samples_challenger), None
    elif rbm_model is not None:
        return None, format_rules(rbm_model, contestation, norm, data, samples_model)
    else:
        return None, None
