from abc import ABCMeta, abstractmethod
from scipy import stats
from data.saved_models.compute_score_coeur_vectorised import *


class Norm():
    __metaclass__ = ABCMeta
    def __init__(self):
        self.type = "objective"

    @abstractmethod
    def _eval(self, undetermined_contest_df):
        raise NotImplementedError("Must override _eval")

    @abstractmethod
    def eval(self, undetermined_contest_df):
        raise NotImplementedError("Must override eval")

    def deviation(self, contest, data, undetermined_contest_df):
        filtered_data = data.loc[contest.target_rule_func(data)]
        return self._eval(undetermined_contest_df).mean() - self._eval(filtered_data).mean()

    def ttest(self, contest, data, undetermined_contest_df):
        filtered_data = data.loc[contest.target_rule_func(data)]
        ttest = stats.ttest_ind(self._eval(undetermined_contest_df), self._eval(filtered_data), equal_var=False)
        return (ttest.statistic, ttest.pvalue)

    def evidence(self, contest, data, undetermined_contest_df):
        filtered_data = data.loc[contest.target_rule_func(data)]
        deviation = self.deviation(contest, data, undetermined_contest_df)
        ttest = stats.ttest_ind(self._eval(undetermined_contest_df), self._eval(filtered_data), equal_var=False)
        return (deviation, len(undetermined_contest_df)), (ttest.statistic, ttest.pvalue)


class Risk(Norm):
    def __init__(self):
        super(Risk, self).__init__()
        self.description = "Risque de décès sur liste d'attente (ICAR)"

    def __str__(self):
        return "Risque de décès sur liste d'attente (ICAR)"

    def _eval(self, undetermined_contest_df):
        return f_icar(undetermined_contest_df)

    def eval(self, undetermined_contest_df):
        return f_icar(undetermined_contest_df).mean()


class DeathRiskNormEmpirical(Norm):
    def __init__(self):
        super(DeathRiskNormEmpirical, self).__init__()
        self.description = "Risque de décès sur liste d'attente"

    def __str__(self):
        return "DeathRiskNormEmpirical"

    def _eval(self, undetermined_contest_df):
        return undetermined_contest_df['ETATLA'] == 'DCD'

    def eval(self, undetermined_contest_df):
        return (undetermined_contest_df['ETATLA'] == 'DCD').mean()


class Comp(Norm):
    def __init__(self):
        super(Comp, self).__init__()
        self.description = "Composante expert adulte/pédiatrique ou pédiatrique standard. "

    def __str__(self):
        return "Points de composante expert"

    def _eval(self, undetermined_contest_df):
        ccb = f_ccb(undetermined_contest_df)
        icar_std = f_icar_std(undetermined_contest_df)
        return ccb * (ccb > icar_std)

    def eval(self, undetermined_contest_df):
        ccb = f_ccb(undetermined_contest_df)
        icar_std = f_icar_std(undetermined_contest_df)
        return (ccb * (ccb > icar_std)).mean()


class RiskPT(Norm):
    def __init__(self):
        super(RiskPT, self).__init__()
        self.description = "Survie post-greffe à un an"

    def __str__(self):
        return "Survie post-greffe à un an"

    def _eval(self, undetermined_contest_df):
        risk = f_risk_post_gref(undetermined_contest_df)
        return risk

    def eval(self, undetermined_contest_df):
        risk = f_risk_post_gref(undetermined_contest_df)
        return risk.mean()


class Match(Norm):
    def __init__(self):
        super(Match, self).__init__()
        self.description = "Facteur de compatibilité"

    def __str__(self):
        return "Compatibilité avec le donneur"

    def _eval(self, undetermined_contest_df):
        risk = matching_coefficient(undetermined_contest_df)
        return risk

    def eval(self, undetermined_contest_df):
        risk = matching_coefficient(undetermined_contest_df)
        return risk.mean()


class Youth(Norm):
    def __init__(self):
        super(Youth, self).__init__()
        self.description = "Âge moyen"

    def __str__(self):
        return "Youth"

    def _eval(self, undetermined_contest_df):
        return - undetermined_contest_df['age_r']

    def eval(self, undetermined_contest_df):
        return (- undetermined_contest_df['age_r']).mean()


class UnderageNorm(Norm):
    def __init__(self):
        super(UnderageNorm, self).__init__()
        self.description = "Proportion de patients mineurs"

    def __str__(self):
        return "UnderageNorm"

    def _eval(self, undetermined_contest_df):
        return undetermined_contest_df['age_r'] <= 18

    def eval(self, undetermined_contest_df):
        return (undetermined_contest_df['age_r'] <= 18).mean()


class WaitNorm(Norm):
    def __init__(self):
        super(WaitNorm, self).__init__()
        self.description = "Durée moyenne d'attente"

    def __str__(self):
        return "WaitNorm"

    def _eval(self, undetermined_contest_df):
        return undetermined_contest_df['da']

    def eval(self, undetermined_contest_df):
        return undetermined_contest_df['da'].mean()


class Transport(Norm):
    def __init__(self):
        super(Transport, self).__init__()
        self.description = "Facteur modèle gravitaire"

    def __str__(self):
        return "Réduction du transport du greffon"

    def _eval(self, undetermined_contest_df):
        return gravity_model(undetermined_contest_df)

    def eval(self, undetermined_contest_df):
        return gravity_model(undetermined_contest_df).mean()


class RarityNorm(Norm):
    def __init__(self):
        super(RarityNorm, self).__init__()
        self.description = "Proportion de patients non-greffés"

    def __str__(self):
        return "RarityNorm"

    def _eval(self, undetermined_contest_df):
        return (1 - (undetermined_contest_df['ETATLA'] == 'GRF'))

    def eval(self, undetermined_contest_df):
        return (1 - (undetermined_contest_df['ETATLA'] == 'GRF')).mean()

