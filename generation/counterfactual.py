import copy
from sampling.selection import *
from generation.utils import *


def example_to_difference(base, counterfactuals):
    cf_list = []
    for row in counterfactuals.iterrows():
        cf = {}
        for col in counterfactuals.columns:
            if row[1][col]!=base.scope[col].values[0]:
                cf[col] = {'old_val': base.scope[col].values[0],
                           'new_val': row[1][col]}
        cf_list.append(cf)
    return cf_list


def find_counterfactual(base, sampling, kwargs_sampling, kwargs_generation):
    """
    Search for n_cf counterfactuals using the sampling method provided.
    :param base: object: the database object (contains in particular the population and the scope of the explanation)
    :param sampling: function: the sampling method
    :param args_init: dict: arguments to onset the search
    :param n_cf: the target number of counterfactuals
    :param target_class: if specified, the counterfactuals are predicted for this class
    :return: pandas.DataFrame: population of n_cf counterfactuals
    """
    # args = {'sigma': 10, 'rdm_distribution': 'lognormal', 'convert_to_binary_class': False}
    # args = {'n_col_distrib': 'normal', 'normal_sigma': 1, 'convert_to_binary_class': False}
    dynamic_arguments = SamplingArguments(kwargs_sampling)
    current_class = base.model(base.scope)[0]
    ongoing = True
    count = 0
    while ongoing:
        # sampling
        samples = sampling(base, **dynamic_arguments.arguments())
        n_all_samples = len(samples)
        # only keep cf candidate with a an output different from the scope
        import pdb
        pdb.set_trace()
        if base.classification:
            samples = samples[samples['output']==current_class]
        else:
            samples = samples[np.abs(samples['output'] - current_class) > np.std(base.y_raw)]
        # heuristic criteria to evaluate the quality of the counterfactual
        print(count)
        if count > 10:
            raise ValueError("No counterfactuals found")
        elif len(samples) > (n_all_samples / 1.2):  # TODO : trouver un critere plus objectif
            dynamic_arguments.narrow()
            args = dynamic_arguments.arguments()
            count += 1
            print("Too broad, too many samples")
        elif len(samples) == 0:  # no cf found, sampling is too restricted
            dynamic_arguments.broaden()
            args = dynamic_arguments.arguments()
            count += 1
            print("Too narrow, no samples")
        else:  # satisfying number of samples found
            pert_base = copy.copy(base)
            pert_base.population = samples
            samples['sim'] = pert_base.pop_similarity_to_x(base.scope, features_subset=samples.drop('output', axis=1).columns)
            samples = samples.sort_values('sim')
            selected_cf = samples.tail(kwargs_generation['n_cf']).copy()
            return example_to_difference(base, selected_cf.drop(['output', 'sim'], axis=1))
