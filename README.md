L'organisation conseillé pour le dossier du code est la suivante : 

```
explicoeur
├── env
└── src
    ├── coeur
    ├── data
    ├── db.sqlite3
    ├── delivering
    ├── generation
    ├── justify
    ├── manage.py
    ├── mysite
    ├── README.md
    ├── requirements.txt
    ├── sampling
    ├── settings
    ├── shared
    ├── templates
    └── waittime

```


En pratique :

```
 mkdir explicoeur
 cd explicoeur
 git clone https://gitlab.inria.fr/chenin/explicoeur.git
 mv explicoeur src
```

Ensuite on crée l'environnement virutel :

```
 virtualenv -p python3 env
 source env/bin/activate
 cd src
 pip install -r requirements.txt
```

Vous devriez désormais pouvoir executer l'application en local avec la commande suivante : 

```python manage.py runserver```

ou bien: 

```python3 manage.py runserver``` 

(en fonction de la version de python par défaut)

Créer le fichier de config local (il est dans le gitignore pour ne pas le mettre en prod)

``` cp explicoeur/settings/production.py explicoeur/settings/local.py ```
