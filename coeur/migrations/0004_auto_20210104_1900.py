# Generated by Django 3.1.3 on 2021-01-04 19:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coeur', '0003_auto_20210104_1840'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contestvalue',
            name='abo_r_value',
            field=models.CharField(blank=True, choices=[('O', 'O'), ('B', 'B'), ('A', 'A'), ('AB', 'AB')], max_length=100, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='age_r_value',
            field=models.FloatField(blank=True, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='bili_value',
            field=models.FloatField(blank=True, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='bnp_value',
            field=models.FloatField(blank=True, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='cat_value',
            field=models.CharField(blank=True, choices=[('False', 'False'), ('O', 'O')], max_length=100, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='cec_value',
            field=models.CharField(blank=True, choices=[('False', 'False'), ('O', 'O')], max_length=100, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='creat_value',
            field=models.FloatField(blank=True, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='da_value',
            field=models.FloatField(blank=True, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='dialyse_value',
            field=models.CharField(blank=True, choices=[('False', 'False'), ('O', 'O')], max_length=100, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='drg_value',
            field=models.FloatField(blank=True, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='poids_r_value',
            field=models.FloatField(blank=True, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='probnp_value',
            field=models.FloatField(blank=True, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='sex_r_value',
            field=models.CharField(blank=True, choices=[('H', 'H'), ('F', 'F')], max_length=100, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='siav_value',
            field=models.CharField(blank=True, choices=[('False', 'False'), ('G', 'G'), ('D', 'D'), ('BV', 'BV')], max_length=100, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='taille_r_value',
            field=models.FloatField(blank=True, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='ttlgp_value',
            field=models.FloatField(blank=True, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='urgence_value',
            field=models.CharField(blank=True, choices=[('False', 'False'), ('XPCP2', 'XPCP2'), ('XPCA', 'XPCA'), ('XPCP1', 'XPCP1')], max_length=100, null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='contestvalue',
            name='y_true_value',
            field=models.FloatField(blank=True, null=True, verbose_name=''),
        ),
    ]
