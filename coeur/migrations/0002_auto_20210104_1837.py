# Generated by Django 3.1.3 on 2021-01-04 18:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coeur', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='contestvalue',
            name='y_true_operator',
            field=models.CharField(blank=True, choices=[('', ''), ('>=', '>='), ('<=', '<=')], max_length=100, null=True, verbose_name='Score'),
        ),
        migrations.AddField(
            model_name='contestvalue',
            name='y_true_value',
            field=models.FloatField(blank=True, default=100, null=True, verbose_name=''),
        ),
    ]
