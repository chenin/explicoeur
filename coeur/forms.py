from crispy_forms.helper import FormHelper

from django.forms import *
from coeur.models import *
from coeur.utils import *
from mysite.settings import *


CHOICE_OUTPUT = (
    ("snacg", "Score coeur (SNAGC)"),
    ("ccp", "Score pondéré (CCP)"),
    ("ccb", "Score brut (CCB)"),
)

CHOICE_EQUIPGRF = (
    ('', ''),
    ('BO5CB', 'BO5CB'),
    ('CA6CA', 'CA6CA'),
    ('CF3CA', 'CF3CA'),
    ('DI3CA', 'DI3CA'),
    ('GR3CA', 'GR3CA'),
    ('LI1XA', 'LI1XA'),
    ('LY3XC', 'LY3XC'),
    ('LY3XM', 'LY3XM'),
    ('MA4CO', 'MA4CO'),
    ('MA4XE', 'MA4XE'),
    ('ML7CP', 'ML7CP'),
    ('MO5CA', 'MO5CA'),
    ('NA2XA', 'NA2XA'),
    ('NN6XA', 'NN6XA'),
    ('PG7CA', 'PG7CA'),
    ('PH7CA', 'PH7CA'),
    ('PI7XA', 'PI7XA'),
    ('PL7XA', 'PL7XA'),
    ('PX7CA', 'PX7CA'),
    ('RE6CA', 'RE6CA'),
    ('RO1CA', 'RO1CA'),
    ('ST2XA', 'ST2XA'),
    ('TO5XA', 'TO5XA'),
    ('TS6CA', 'TS6CA'),
)

# Form definition is put here because it needs access to DATA to validate form
class DateForm(Form):
    date_field = DateField(label="Date", help_text="Laisser vide pour une date au hasard", required=False,
                                 widget=TextInput(attrs={'type': 'date'}))


class EquipeGrf(Form):
    equipgrf_field = ChoiceField(
        label="Code équipe",
        choices=CHOICE_EQUIPGRF,
        required=False,
    )

class OutputFeaturesForm(Form):
    output = ChoiceField(label="Sortie", choices=CHOICE_OUTPUT, initial=CHOICE_OUTPUT[0])
    urgence = BooleanField(label=VAR_NAME_DICT['urgence'], initial=True, required=False)
    age_r = BooleanField(label=VAR_NAME_DICT['age_r'], initial=True, required=False)
    sex_r = BooleanField(label=VAR_NAME_DICT['sex_r'], initial=True, required=False)
    abo_r = BooleanField(label=VAR_NAME_DICT['abo_r'], initial=True, required=False)
    taille_r = BooleanField(label=VAR_NAME_DICT['taille_r'], initial=True, required=False)
    poids_r = BooleanField(label=VAR_NAME_DICT['poids_r'], initial=True, required=False)
    probnp = BooleanField(label=VAR_NAME_DICT['probnp'], initial=True, required=False)
    creat = BooleanField(label=VAR_NAME_DICT['creat'], initial=True, required=False)
    bili = BooleanField(label=VAR_NAME_DICT['bili'], initial=True, required=False)
    drg = BooleanField(label=VAR_NAME_DICT['drg'], initial=True, required=False)
    cec = BooleanField(label=VAR_NAME_DICT['cec'], initial=True, required=False)
    siav = BooleanField(label=VAR_NAME_DICT['siav'], initial=True, required=False)
    dialyse = BooleanField(label=VAR_NAME_DICT['dialyse'], initial=True, required=False)
    cat = BooleanField(label=VAR_NAME_DICT['cat'], initial=True, required=False)
    da = BooleanField(label=VAR_NAME_DICT['da'], initial=True, required=False)
    ttlgp = BooleanField(label=VAR_NAME_DICT['ttlgp'], initial=True, required=False)
    assist_bonus = BooleanField(label=VAR_NAME_DICT['assist_bonus'], initial=True, required=False)

#
# class ContestValueForm(ModelForm):
#     class Meta:
#         model = ContestValue
#         exclude = ['file', 'target_output']
#
#     def __init__(self, *args, **kwargs):
#         file = kwargs.pop('file', None)
#         super(ContestValueForm, self).__init__(*args, **kwargs)
#         self.file = file
#
#     helper = FormHelper()
#     # helper.form_class = 'form-horizontal'
#     helper.label_class = 'col-lg-12'
#     helper.field_class = 'col-lg-12'
#     helper.form_show_labels = False
#
#     def clean(self, *args, **kwargs):
#         """
#         In here you can validate the two fields
#         raise ValidationError if you see anything goes wrong.
#         for example if you want to make sure that field1 != field2
#         """
#         cleaned_data = super().clean()
#         f_names = [f.name for f in ContestValue._meta.get_fields()]
#         model_fields = {name.replace('_operator', '').replace('_value', '')
#                         for name in f_names if '_operator' in name or '_value' in name}
#         for field in list(model_fields):
#             field_op = cleaned_data[field + '_operator']
#             field_val = cleaned_data[field + '_value']
#             file = self.file.to_dict('records')[0]
#             if field == "y_true":
#                 if (field_op is None) or (field_val is None):
#                     self._errors[field + '_value'] = [
#                         "Ce champs doit être rempli"]  # Will raise a error message
#             elif (field_op is None) != (field_val is None):
#                 self._errors[field + '_value'] = ["L'opérateur ET la valeur doivent être remplis ou laissés vides tous les deux"]  # Will raise a error message
#             elif ((field_op is not None) and
#                 not convert_operator(field_op)(file[field], field_val)):
#                 self._errors[field + '_value'] = ["Le dossier doit satisfaire cette contrainte"]  # Will raise a error message
#         return cleaned_data
