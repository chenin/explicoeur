from django.views.generic import TemplateView
from django.shortcuts import render
from django.shortcuts import redirect
from django.http import  HttpResponseRedirect
from bokeh.models.widgets import DataTable, TableColumn, HTMLTemplateFormatter
from bokeh.models import DatetimeTickFormatter

from waittime.estimation import *
from data.db_example import *
from delivering.f_specific_plot import *
from generation.features_specific import *
from generation.rule_based_model import *
from justify.rule_based_justifications import *
from sampling.selection import *
from coeur.forms import *
from justify.norm import *

ABMBASE = ABMBase()
DATA = copy.deepcopy(ABMBASE.population)
DATA['snacg'] = np.around(copy.deepcopy(ABMBASE.y_raw), 2)
DATA['y_true'] = DATA['snacg']
DATA['rank'] = f_rank(DATA)
DATA['ccb'] = np.around(f_ccb(DATA), 2)
DATA['ccp'] = np.around(f_ccp(DATA), 2)
DATA['id_decision'] = DATA['CB_ID'].astype(str) + '_' + DATA['NUMDON_ANON'].astype(str)

ABMBASE.y_range = ABMBASE.compute_range_wo_outliers(DATA)
Y_RANGES = ABMBASE.y_range

DEFAULT_FEATURES = [
    "urgence",
    "age_r", "sex_r", "abo_r", "taille_r", "poids_r",
    "probnp", "creat", "bili",
    "drg", "cec", "siav", "dialyse", "cat",
    "da",
    "ttlgp",
    "assist_bonus",
]

PERM_SAMPLING = Permutation(ABMBASE, {'size': 1000, 'n_col_distrib': 'uniform'},
                          coupled_var=[('cec', 'delay_cec'), ("urgence", "daurg", 'xpc')],
                          actionable_features=DEFAULT_FEATURES, )


# Form definition is put here because it needs access to DATA to validate form
class PatientIdForm(Form):
    id_patient = IntegerField(label="Identifiant Patient", help_text="Laisser vide pour patient au hasard", required=False)

    def clean(self, *args, **kwargs):
        """
        In here you can validate the two fields
        raise ValidationError if you see anything goes wrong.
        for example if you want to make sure that field1 != field2
        """
        cleaned_data = super().clean()
        id_patient = cleaned_data['id_patient']
        if id_patient:
            if id_patient not in DATA['NATT_ANON'].values:
                self._errors['id_patient'] = ["Cet identifiant ne correspond à aucun patient"]  # Will raise a error message
        return cleaned_data


# Form definition is put here because it needs access to DATA to validate form
class DecisionIdForm(Form):
    id_bilan = IntegerField(label="Identifiant Bilan", required=False)
    id_donneur = IntegerField(label="Identifiant Donneur", help_text="Laisser vide pour un couple patient/donneur au hasard", required=False)

    def clean(self, *args, **kwargs):
        """
        In here you can validate the two fields
        raise ValidationError if you see anything goes wrong.
        for example if you want to make sure that field1 != field2
        """
        cleaned_data = super().clean()
        id_bilan = cleaned_data['id_bilan']
        id_donneur = cleaned_data['id_donneur']
        if id_bilan and id_donneur:
            if str(id_bilan) + '_' + str(id_donneur) not in DATA['id_decision'].values:
                self._errors['id_donneur'] = ["Ces identifiants ne correspondent à aucune décision enregistrée"]  # Will raise a error message
        return cleaned_data


class OutputFeatureView(TemplateView):
    template_name = "home.html"

    def post(self, request):
        form = OutputFeaturesForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            request.session['output'] = data['output']
            ABMBASE.model = ABMBASE.load_model(which=data['output'])
            request.session['features'] = [key for key, val in data.items() if val==True]
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


# Create your views here.
class StaticView(TemplateView):
    template_name = "home.html"

    def post(self, request, **kwargs):
        form_date = DateForm(request.POST)
        form_patient = PatientIdForm(request.POST)
        form_decision = DecisionIdForm(request.POST)
        if 'id_patient' in request.POST.keys():
            if form_patient.is_valid():
                id_patient = form_patient.cleaned_data['id_patient']
                if id_patient:
                    return redirect('patient', id_patient=id_patient)
                else:
                    return redirect("patient")
        if 'date_field' in request.POST.keys() and form_date.is_valid():
            date = form_date.cleaned_data['date_field']
            if date:
                return redirect('waitlist', date=dt.datetime.strftime(date, '%d%m%Y'))
            else:
                return redirect("waitlist")
        if 'id_donneur' in request.POST.keys() and form_decision.is_valid():
            id_donneur = form_decision.cleaned_data['id_donneur']
            id_bilan = form_decision.cleaned_data['id_bilan']
            if id_bilan and id_donneur:
                return redirect('decision_explanation', id_decision=str(id_bilan) + '_' + str(id_donneur))
            else:
                return redirect("decision_explanation")
        context = {'form_patient': form_patient, 'form_date': form_date, 'form_decision': form_decision}
        return render(request, 'home.html', context)

    def get_context_data(self, **kwargs):
        context = {'form_patient': PatientIdForm, 'form_date': DateForm, 'form_decision': DecisionIdForm}
        return context


class PatientView(TemplateView):
    template_name = "patient_view.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if kwargs.get('id_patient'):
            patient_data = DATA[DATA['NATT_ANON'] == int(kwargs['id_patient'])]
        else:
            rdn_pat = np.random.choice(DATA.NATT_ANON.unique())
            patient_data = DATA[DATA['NATT_ANON'] == rdn_pat]
        context['id_patient'] = str(patient_data['NATT_ANON'].values[0])
        features = copy.deepcopy(self.request.session.get('features', DEFAULT_FEATURES))
        output = copy.deepcopy(self.request.session.get('output', 'snacg'))
        all_plots = []

        # TABLE PATIENT
        patient_fixed_info = ['age_r', 'sex_r', 'abo_r', 'poids_r', 'taille_r']
        patient_info = patient_data[patient_fixed_info].replace(CATE_VALUE_DICT).sample(1).to_dict('series')

        title_patient = Div(text="Informations patient", height=30, style={'font-size': '150%'})

        source = ColumnDataSource(patient_info)
        columns = [
            TableColumn(field=feat, title=VAR_NAME_DICT_R[feat], width=None) for feat in patient_fixed_info
        ]
        patient_table = DataTable(source=source, columns=columns, height=30,
                                  autosize_mode='fit_columns', index_position=None)

        all_plots.append([title_patient])
        all_plots.append([patient_table])


        events = {'urgence': [], 'CB_ID': [], 'drg': [], 'cec': [], 'cat': [], 'siav': []}
        for item in patient_data.iterrows():
            for event_col in [('urgence', 'DURG'), ('CB_ID', 'DATB_ANON'), ('drg', 'DDRG2_ANON'), ('cec', 'DCEC'),
                              ('cat', 'DATB_ANON'), ('siav', 'DAV2_ANON')]:
                if not (pd.isnull(item[1][event_col[0]]) or not item[1][event_col[0]] or item[1][event_col[0]] in [
                    'False', 'N']):
                    events[event_col[0]].append(item[1][event_col[1]])

        events['bilan'] = events.pop("CB_ID")

        etat_last_record = patient_data.loc[patient_data['NEXT_DINS'].idxmax()]
        events[etat_last_record['ETATLA']] = [etat_last_record['NEXT_DINS']]

        for k, v in events.items():
            events[k] = list(set(v))

        all_dates = [item for sublist in events.values() for item in sublist]
        min_date, max_date = min(all_dates), max(all_dates)
        margin = (max_date - min_date) / 10.

        factors = [VAR_NAME_DICT[var] for var in events.keys()]
        x = list(events.values())

        dot = figure(title="Événements",
                     tools="",
                     toolbar_location=None,
                     y_range=factors,
                     x_axis_type='datetime',
                     x_range=(min_date - margin, max_date + margin))

        # x-axis date formatting
        dot.xaxis.formatter = DatetimeTickFormatter(days="%d/%m/%y",
                                                    months="%d/%m/%y",
                                                    hours="%d/%m/%y",
                                                    minutes="%d/%m/%y")

        # dot.segment(0, factors, x, factors, line_width=2, line_color="green", )
        dot.circle(x[0], [factors[0]] * len(x[0]), size=12, fill_color="red", line_color="black", line_width=2, )
        dot.circle(x[1], [factors[1]] * len(x[1]), size=12, fill_color="purple", line_color="black", line_width=2, )
        dot.circle(x[2], [factors[2]] * len(x[2]), size=12, fill_color="green", line_color="black", line_width=2, )
        dot.circle(x[3], [factors[3]] * len(x[3]), size=12, fill_color="green", line_color="black", line_width=2, )
        dot.circle(x[4], [factors[4]] * len(x[4]), size=12, fill_color="blue", line_color="black", line_width=2, )
        dot.circle(x[5], [factors[5]] * len(x[5]), size=12, fill_color="orange", line_color="black", line_width=2, )
        dot.circle(x[6], [factors[6]] * len(x[6]), size=12, fill_color="red", line_color="black", line_width=2, )

        TOOLS = "save,tap"

        all_plots.append([dot])
        for col in ['probnp', 'creat', 'bili']:
            # figure probnp
            p = figure(
                title=VAR_NAME_DICT[col],
                y_range=Y_RANGES[col],
                tools=TOOLS,
                x_axis_type='datetime',
                x_range=(min_date - margin, max_date + margin),
                )
            data_plot = patient_data[[col, 'dt_' + col]].drop_duplicates().dropna().sort_values('dt_' + col)
            x, y = data_plot['dt_' + col], data_plot[col]
            p.circle(x, y)
            p.line(x, y)
            # x-axis date formatting
            p.xaxis.formatter = DatetimeTickFormatter(days="%d/%m/%y",
                                                        months="%d/%m/%y",
                                                        hours="%d/%m/%y",
                                                        minutes="%d/%m/%y")
            all_plots.append([p])

        text_div = Div(text="""Information sur les propositions de greffons""", height=30, style={'font-size': '150%'})

        all_plots.append([text_div])

        data_plot = patient_data[[output, 'rank', 'DATPREL_ANON', 'id_decision']].drop_duplicates().dropna()
        data_plot = data_plot.sort_values('DATPREL_ANON')
        data_plot = data_plot[data_plot[output] > 0]
        colors = (data_plot['rank'] < 3).replace(True, 'red').replace(False, "#3288bd")

        source = ColumnDataSource(
            data=dict(
                dates=data_plot['DATPREL_ANON'],
                score=data_plot[output],
                rank=data_plot['rank'],
                propal_colors=colors,
                id_link=data_plot['id_decision'],
            )
        )

        p = figure(title='Valeur {name} ({name} > 0)'.format(name=VAR_NAME_DICT[output]),
                   y_range=Y_RANGES[output],
                   tools=TOOLS,
                   x_axis_type='datetime',
                   x_range=(min_date - margin, max_date + margin),
                   )
        p.square('dates', 'score', source=source, size=10)
        p.xaxis.formatter = DatetimeTickFormatter(days="%d/%m/%y",
                                                  months="%d/%m/%y",
                                                  hours="%d/%m/%y",
                                                  minutes="%d/%m/%y")

        all_plots.append([p])

        p = figure(title='Rang SNACG ({name} > 0)'.format(name=VAR_NAME_DICT[output]),
                   y_range=(300, -5),  # reserved axis for rank (top rank top page)
                   tools=TOOLS,
                   x_axis_type='datetime',
                   x_range=(min_date - margin, max_date + margin),
                   )
        p.square('dates', 'rank', source=source, color='propal_colors', size=10)
        p.xaxis.formatter = DatetimeTickFormatter(days="%d/%m/%y",
                                                  months="%d/%m/%y",
                                                  hours="%d/%m/%y",
                                                  minutes="%d/%m/%y")

        all_plots.append([p])

        title_score = Div(text="Tableau des scores", height=30, style={'font-size': '150%'})

        url = "/decision/explanation/@id_link/"
        taptool = p.select(type=TapTool)
        taptool.callback = OpenURL(url=url)

        features += [output, 'DATPREL_ANON', 'age_d', 'sex_d', 'abo_d', 'poids_d', 'taille_d']

        patient_data = patient_data.sort_values('DATPREL_ANON')
        patient_data['DATPREL_ANON'] = patient_data['DATPREL_ANON'].apply(lambda x: dt.datetime.strftime(x, '%d/%m/%Y'))

        patient_data = event_to_one_column(patient_data)
        remain_feat = [x for x in patient_data.columns if (x in features or x == 'events') and x not in patient_fixed_info]
        remain_feat.insert(0, remain_feat.pop(-1))

        patient_data.replace(CATE_VALUE_DICT, inplace=True)
        table_data = patient_data.to_dict('series')
        source = ColumnDataSource(table_data)

        url_template = self.request.build_absolute_uri('/decision/explanation/')
        bokeh_url_template = HTMLTemplateFormatter(template='<a href="' + url_template + '<%= value %>"/> <%= value %> </a>')
        columns = [
            TableColumn(field='id_decision', title='Id decision', formatter=bokeh_url_template)
        ]
        columns += [
            TableColumn(field=feat, title=VAR_NAME_DICT[feat]) for feat in remain_feat
        ]

        decision_table = DataTable(source=source, columns=columns, index_position=None)

        all_plots.append([title_score])
        all_plots.append([decision_table])

        plot = gridplot(all_plots, sizing_mode="scale_width", plot_height=20 * len(all_plots))

        script, div = components(plot)
        context['script'] = script
        context['div'] = div
        return context


class DecisionExplanationView(TemplateView):
    template_name = "decision_explanation_view.html"

    def get_context_data(self, **kwargs):
        top_page_plots = []
        orange = RdYlGn[3][2]
        green = RdYlGn[3][0]
        context = super().get_context_data(**kwargs)
        if kwargs.get('id_decision'):
            decision_data = DATA[DATA['id_decision'] == kwargs['id_decision']]
        else:
            rdn_decision = np.random.choice(DATA.id_decision)
            decision_data = DATA[DATA['id_decision'] == rdn_decision]
        features = copy.deepcopy(self.request.session.get('features', DEFAULT_FEATURES))
        output = copy.deepcopy(self.request.session.get('output', 'snacg'))
        context['output_name'] = VAR_NAME_DICT[output]

        ABMBASE.scope = decision_data
        id_decision = decision_data['id_decision'].values[0]
        context['id_decision'] = id_decision
        context['output_value'] = decision_data[output].values[0]

        scope_table = decision_data.copy()
        scope_table.fillna('///', inplace=True)

        text_div = Div(text="Informations sur le candidat et le donneur", height=30, style={'font-size': '150%'})

        # TABLE BILAN
        bilan_var = ['probnp', 'bili', 'creat']
        bilan_labels = [VAR_NAME_DICT[var] for var in bilan_var]
        bilan_dates = scope_table[['dt_probnp', 'dt_bili', 'dt_creat']].values[0]
        bilan_values = scope_table[bilan_var].values[0]
        bilan_data = dict(label=bilan_labels, value=bilan_values, date=bilan_dates)
        source = ColumnDataSource(bilan_data)
        columns = [
            TableColumn(field="label", title="Type", width=None),
            TableColumn(field="value", title="Valeur", width=None),
            TableColumn(field="date", title="Date", formatter=DateFormatter(format='%d/%m/%Y'), width=None),
        ]
        bilan_plot = DataTable(source=source, columns=columns, width=400, height=280,
                               autosize_mode='fit_columns', index_position=None,)

        # TABLE APPARIEMENT
        match_var_r = ['abo_r', 'age_r', 'sex_r', 'taille_r', 'poids_r', 'ttlgp', 'assist_bonus']
        match_var_d = ['abo_d', 'age_d', 'sex_d', 'taille_d', 'poids_d', 'ttlgp', 'assist_bonus']
        match_labels = [VAR_NAME_DICT[var].replace(" receveur", "") for var in match_var_r]
        match_r = scope_table[match_var_r].replace(CATE_VALUE_DICT).values[0]
        match_d = scope_table[match_var_d].replace(CATE_VALUE_DICT).values[0]
        bilan_data = dict(match_labels=match_labels, match_r=match_r, match_d=match_d)
        source = ColumnDataSource(bilan_data)
        columns = [
            TableColumn(field="match_labels", title="Type", width=None),
            TableColumn(field="match_r", title="Receveur", width=None),
            TableColumn(field="match_d", title="Donneur", width=None),
        ]
        match_plot = DataTable(source=source, columns=columns, width=400, height=280,
                               autosize_mode='fit_columns', index_position=None)

        # TABLE "EVENTS"
        event_var = ['cec', 'urgence', 'siav', 'dialyse', 'drg']
        events_labels = [VAR_NAME_DICT[var] for var in event_var]
        events_values = scope_table[event_var].replace(CATE_VALUE_DICT)
        events_values = events_values.replace("False", "Non").values[0]
        bilan_data = dict(events_labels=events_labels, events_values=events_values)
        source = ColumnDataSource(bilan_data)
        columns = [
            TableColumn(field="events_labels", title="Type", width=None),
            TableColumn(field="events_values", title="Valeur", width=None),
        ]
        event_plot = DataTable(source=source, columns=columns, width=400, height=280,
                               autosize_mode='fit_columns', index_position=None)

        top_page_plots.append([text_div])
        top_page_plots.append([event_plot, bilan_plot, match_plot])

        # PLOT FEATURE IMPORTANCES (IBEX)
        # Compute feature importances
        model = ABMBASE.load_model(which=output)
        samples = PERM_SAMPLING.y0_conditioned_sampling(decision_data, model)
        samples = samples[features + ['output']]
        fi = lasso_regression(ABMBASE, decision_data, samples, alpha=1)
        fi_std = lasso_coef_std(ABMBASE, samples, alpha=1)
        # filter not significant FI
        fi = {k: v for k, v in fi.items() if abs(v) > 2 * fi_std[k]}
        if len(fi) > 0:
            # Convert to bokeh-compatible data format
            features_name_fi_plot = [convert_feature_name(label) for label in fi.keys()]
            values_fi_plot = list(fi.values())
            values_fi_plot, features_name_fi_plot = zip(*sorted(zip(values_fi_plot, features_name_fi_plot)))  # sort by feature importance value
            colors = [orange if val < 0 else green for val in values_fi_plot]

            title_explanations = Div(text="Explications", height=30, style={'font-size': '150%'})

            TOOLS = "save"
            # instantiating the figure object
            fi_plot = figure(title="Importances relatives des variables",
                             y_range=FactorRange(factors=features_name_fi_plot),
                             plot_height=max(len(values_fi_plot) * 30, 200),
                             max_width=800,
                             tools=TOOLS,
                             align='center')

            source = ColumnDataSource(dict(y=features_name_fi_plot, right=values_fi_plot, color=colors))

            glyph = HBar(y="y", right="right", fill_color='color', left=0, height=0.3)
            fi_plot.add_glyph(source, glyph)
            top_page_plots.append(fi_plot)

        grid = layout(top_page_plots, sizing_mode="scale_width")

        script, div = components(grid)
        context['script'] =script
        context['div'] = div

        # RULE-BASED EXPLANATION
        samples = PERM_SAMPLING.y500_conditioned_sampling(decision_data, model)
        samples = samples[features + ['output']]
        samples = samples.dropna()
        rbms = rule_based_model(ABMBASE, decision_data, samples, model, actionable_features=features, min_accuracy=.9)
        rbm = [rule_model for rule_model in rbms if rule_model!=([], 0, -1)]
        if len(rbm) > 0:
            rbm = [(rbm_format_rule(pred), test_rule(pred, scope_table)) for pred in rbm[0][0]]
            context['rules'] = rbm


        # PLOT JUSTIFICATION ("NORM-BASED EXPLANATIONS")
        bottom_page_plots = []
        title_justification = Div(text="Justifications", height=30, style={'font-size': '150%'})
        id_donneur = decision_data['NUMDON_ANON'].values[0]
        active_list = DATA[DATA['NUMDON_ANON'] == id_donneur].copy()
        norms = [Risk(), Comp(), RiskPT(), Match(), Transport()]
        for norm in norms:
            norm_values = norm._eval(active_list)
            # compare justification values with average active_list value
            active_list.loc[:, str(norm)] = (norm_values - norm_values.mean()) / norm_values.std()
        patient = active_list[active_list['id_decision'] == id_decision].to_dict('records')[0]
        label_justif_plot = [str(norm) for norm in norms][::-1]
        val_justif_plot = [patient[norm] for norm in label_justif_plot]
        colors = [orange if val < 0 else green for val in val_justif_plot]

        TOOLS = "save"
        # instantiating the figure object
        justif_plot = figure(title="Valeurs des normes comparées à la liste d'attente",
                             y_range=FactorRange(factors=label_justif_plot),
                             plot_height=max(len(val_justif_plot) * 30, 200),
                             max_width=800,
                             tools=TOOLS,
                             align='center')

        source = ColumnDataSource(dict(y=label_justif_plot, right=val_justif_plot, color=colors))
        glyph = HBar(y="y", right="right", fill_color='color', left=0, height=0.3)
        justif_plot.add_glyph(source, glyph)

        bottom_page_plots.append([title_justification])
        bottom_page_plots.append([justif_plot])

        grid = layout(bottom_page_plots, sizing_mode="scale_width")

        script_bottom, div_bottom = components(grid)
        context['script_bottom'] =script_bottom
        context['div_bottom'] = div_bottom

        context['scope'] = scope_table.to_dict('records')[0]

        return context


class Rankings(TemplateView):
    template_name = "rankings.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        output = self.request.session.get('output', 'snacg')
        context['output_name'] = VAR_NAME_DICT[output]
        table_feat = copy.deepcopy(self.request.session.get('features', DEFAULT_FEATURES))
        table_feat = [x for x in table_feat if x in ABMBASE.features_recipient]
        table_feat.append('id_decision')
        table_feat.append(output)
        table_feat.append('rank')
        id_donneur = kwargs.get('id_donneur', DATA['NUMDON_ANON'].sample(1).values[0])
        active_list = DATA[DATA['NUMDON_ANON'] == id_donneur].copy()
        don_info = active_list[ABMBASE.features_giver].replace(CATE_VALUE_DICT).sample(1).to_dict('series')

        title_donneur = Div(text="Informations donneur", height=30, style={'font-size': '150%'})

        # TABLE DONNEUR
        source = ColumnDataSource(don_info)
        columns = [
            TableColumn(field=feat, title=VAR_NAME_DICT_D[feat]) for feat in ABMBASE.features_giver
        ]
        donneur_table = DataTable(source=source, columns=columns, height=30, index_position=None)

        str_date = active_list['DATPREL_ANON'].apply(lambda x: x.strftime('%d/%m/%Y')).values[0]
        title_rankings = Div(text="Classement pour le prélèvement du " + str_date, height=30, style={'font-size': '150%'})

        context['date_prel'] = str_date

        # Classements
        active_list = active_list.sort_values('rank')[table_feat]
        active_list = event_to_one_column(active_list)
        remain_feat = [x for x in active_list.columns if x in table_feat or x == 'events']
        remain_feat.insert(0, remain_feat.pop(-1))

        active_list = active_list.replace(CATE_VALUE_DICT)
        rankings_data = active_list[remain_feat].to_dict('series')

        source = ColumnDataSource(rankings_data)

        url_template = self.request.build_absolute_uri('/decision/explanation/')
        columns = [
            TableColumn(
                field='id_decision',
                title='Id décision',
                formatter=HTMLTemplateFormatter(
                    template='<a href="' + url_template + '<%= value %>"/> <%= value %> </a>'),
                width=None)
        ]

        columns += [
            TableColumn(field=feat, title=VAR_NAME_DICT_R[feat], width=None) for feat in remain_feat if feat!='id_decision'
        ]

        rankings_table = DataTable(source=source, columns=columns, autosize_mode="fit_columns", index_position=None)

        grid = layout([
            [title_donneur],
            [donneur_table],
            [title_rankings],
            [rankings_table],
        ], sizing_mode="scale_width")

        script, div = components(grid)
        context['script'] =script
        context['div'] = div
        return context


class Waitlist(TemplateView):
    template_name = "waitlist.html"

    def post(self, request, **kwargs):
        form_date = DateForm(request.POST)
        form_equipgrf = EquipeGrf(request.POST)
        if 'date_field' in request.POST.keys() and form_date.is_valid():
            self.request.session.pop("date", 'None')
            date = form_date.cleaned_data['date_field']
            if date:
                return redirect('waitlist', date=dt.datetime.strftime(date, '%d%m%Y'))
            else:
                return redirect("waitlist")
        if 'clear_filter' in request.POST.keys():
            del self.request.session['equipgrf']
            if kwargs:
                return redirect("waitlist", **kwargs)
            else:
                return redirect("waitlist")
        elif 'equipgrf_field' in request.POST.keys() and form_equipgrf.is_valid():
            equipgrf = form_equipgrf.cleaned_data['equipgrf_field']
            if equipgrf or equipgrf == '':
                self.request.session['equipgrf'] = equipgrf
            if kwargs:
                return redirect("waitlist", **kwargs)
            else:
                return redirect("waitlist")
        return render(request, 'waitlist.html', {'form_date': form_date, 'form_equipgrf': form_equipgrf})

    def get_context_data(self, date=None, **kwargs):
        context = super().get_context_data(**kwargs)
        output = self.request.session.get('output', 'snacg')
        context['output_name'] = VAR_NAME_DICT[output]
        features = copy.deepcopy(self.request.session.get('features', DEFAULT_FEATURES))
        features = [x for x in features if x not in ABMBASE.features_giver]
        features.append('NATT_ANON')
        features.insert(0, 'EQUIPGRF')
        features.append('proba_3')
        features.append('proba_7')
        features.append('proba_14')
        features.append('proba_30')
        if date is not None:
            date = dt.datetime.strptime(date, '%d%m%Y')
        else:
            if self.request.session.get('date', False):
                date = self.request.session['date']
                date = dt.datetime.strptime(date, '%d%m%Y')
            else:
                date = DATA['DATB_ANON'].sample(1).values[0]
                date = pd.to_datetime(date)
                self.request.session['date'] = date.strftime('%d%m%Y')

        context['daydate'] = pd.to_datetime(date)
        form_date = DateForm(initial={'date_field': date.strftime('%Y-%m-%d')})
        form_equipgrf = EquipeGrf(initial={'equipgrf_field': self.request.session.get('equipgrf', '')})
        context['form_date'] = form_date
        context['form_equipgrf'] = form_equipgrf

        filtered_dates = DATA[(DATA['DATB_ANON'] <= date) & (date <= DATA['NEXT_DINS'])]
        if len(filtered_dates) > 0:
            estimations = estimate_wt_in_and_out_model(DATA, date)
            estimations.sort_values("proba_3", inplace=True, ascending=False)
            estimations = (estimations * 100).apply(round).astype(int).astype(str) + ' %'
            waitlist = pd.merge(DATA, estimations, on="NATT_ANON", how="right")
            waitlist = waitlist[(waitlist['DATB_ANON'] < date) & (date < waitlist['NEXT_DINS'])]
            waitlist = waitlist.drop_duplicates(['NATT_ANON'])

            if self.request.session.get('equipgrf', ''):
                waitlist = waitlist[waitlist['EQUIPGRF'] == self.request.session['equipgrf']]
                context['equipgrf'] = self.request.session['equipgrf']

            waitlist = event_to_one_column(waitlist)
            remain_feat = [x for x in waitlist.columns if x in features or x == 'events']
            remain_feat.insert(1, remain_feat.pop(-1))
            waitlist = waitlist.replace(CATE_VALUE_DICT)
            waitlist_data = waitlist[remain_feat].to_dict('series')

            source = ColumnDataSource(waitlist_data)
            url_template = self.request.build_absolute_uri('/patient/')
            columns = [
                TableColumn(
                    field='NATT_ANON',
                    title='Id patient',
                    formatter=HTMLTemplateFormatter(template='<a href="' + url_template +  '<%= value %>"/> <%= value %> </a>'),
                    width=None)
            ]
            columns += [
                TableColumn(field=feat, title=VAR_NAME_DICT_R[feat], width=None)
                for feat in remain_feat if feat != 'NATT_ANON'
            ]

            waitlist = DataTable(source=source, columns=columns, autosize_mode="fit_columns", index_position=None)

            grid = layout([
                [waitlist],
            ], sizing_mode="scale_width")

            script, div = components(grid)
            context['script'] =script
            context['div'] = div
        return context


class GlobalExplanation(TemplateView):
    template_name = "decision_tree.html"

    def get_context_data(self, date=None, **kwargs):
        context = super().get_context_data(**kwargs)
        output_label = self.request.session.get('output', 'snacg')
        model = ABMBASE.load_model(which=output_label)
        output_name = VAR_NAME_DICT[output_label]
        context['output_name'] = output_name
        # features = self.request.session.get('features', DEFAULT_FEATURES)
        # base = copy.deepcopy(ABMBASE)
        # samples = identity_sampling(base, model, actionable_features=features)
        # samples = samples.dropna()
        # samples.reset_index(inplace=True)
        # output = samples['output']
        #
        # explanation_data = decision_tree(base, samples[features + ['output']], min_accuracy=.5)
        # if len(features) > 0:
        #     non_cate_features = [x for x in base.feature_names if x not in base.categorical_features]
        #     ordered_col_names = list(base.categorical_features) + list(non_cate_features)
        #     ordered_act_features = [f for f in ordered_col_names if f in features]
        #     f_names = [base.encoded_names_dict[f] for f in ordered_act_features]
        #     f_names = [item for sublist in f_names for item in sublist]
        # else:
        #     f_names = base.encoded_feature_names
        # if getattr(base, 'categorical_features', False):
        #     encoded_samples = base.one_hot_encode(samples)
        #     encoded_samples['output'] = output
        # plot = display_decision_tree_bokeh(explanation_data, encoded_samples, f_names, output_label)
        # script, div = components(plot)
        # dump((script, div), "data/saved_models/script_div_dt_" + output_label + ".joblib")
        script, div = load("data/saved_models/script_div_dt_" + output_label + ".joblib")
        context['script'] =script
        context['div'] = div
        return context


class PartialDepExplView(TemplateView):
    template_name = "partial_dep.html"

    def get_context_data(self, date=None, **kwargs):
        context = super().get_context_data(**kwargs)
        output = self.request.session.get('output', 'snacg')
        model = ABMBASE.load_model(output)
        context['output_name'] = VAR_NAME_DICT[output]
        features = copy.deepcopy(self.request.session.get('features', DEFAULT_FEATURES))
        base = copy.deepcopy(ABMBASE)
        base.init_scope('all')
        pdp_data = partial_dependance(base,
                                      output,
                                      model,
                                      actionable_features=features,
                                      # load_saved=False,
                                      )
        plot_list = plot_bokeh_pdp(base, pdp_data, features=features, output=output)
        plot = gridplot(plot_list, sizing_mode="scale_width", plot_width=400)

        script, div = components(plot)
        context['script'] =script
        context['div'] = div
        return context


# class ChallengeView(TemplateView):
#
#     template_name = "challenge.html"
#     def get_context_data(self, id_decision, **kwargs):
#         context = super().get_context_data(**kwargs)
#         ABMBASE.scope = DATA[DATA['id_decision'] == id_decision]
#         output = "rank"
#         # output = self.request.session.get('output', 'score')
#         context['output_name'] = VAR_NAME_DICT[output]
#         features = self.request.session.get('features', DEFAULT_FEATURES)
#         scope_table = ABMBASE.scope.copy()
#         scope_table['y_true'] = scope_table[output]
#         context['decision_data'] = scope_table
#         form = ContestValueForm(file=scope_table)
#         context['form'] = form
#         context['file'] = scope_table.to_dict('records')[0]
#
#         # TABLE BILAN
#         bilan_labels = ['NT-probnp', 'Bilirubine', 'Créatinine']
#         bilan_dates = scope_table[['dt_probnp', 'dt_bili', 'dt_creat']].values[0]
#         bilan_values = scope_table[['probnp', 'bili', 'creat']].values[0]
#         bilan_data = dict(label=bilan_labels, value=bilan_values, date=bilan_dates)
#         source = ColumnDataSource(bilan_data)
#         columns = [
#             TableColumn(field="label", title="Type"),
#             TableColumn(field="value", title="Valeur"),
#             TableColumn(field="date", title="Date", formatter=DateFormatter(format='%d/%m/%Y')),
#         ]
#         bilan_plot = DataTable(source=source, columns=columns, width=400, height=280)
#
#         # TABLE APPARIEMENT
#         match_labels = ['Groupe ABO', 'Âge', 'Sexe', 'Taille', 'Poids', "Distance (minutes)"]
#         match_r = scope_table[['abo_r', 'age_r', 'sex_r', 'taille_r', 'poids_r', 'ttlgp']].values[0]
#         match_d = scope_table[['abo_d', 'age_d', 'sex_d', 'taille_d', 'poids_d', 'ttlgp']].values[0]
#         bilan_data = dict(match_labels=match_labels, match_r=match_r, match_d=match_d)
#         source = ColumnDataSource(bilan_data)
#         columns = [
#             TableColumn(field="match_labels", title="Type"),
#             TableColumn(field="match_r", title="Receveur"),
#             TableColumn(field="match_d", title="Donneur"),
#         ]
#         match_plot = DataTable(source=source, columns=columns, width=400, height=280)
#
#         # TABLE "EVENTS"
#         events_labels = ['CEC', 'Composante expert', 'SIAV', 'Dialyse', 'Drogue']
#         events_values = scope_table[['cec', 'urgence', 'siav', 'dialyse', 'drg']]
#         events_values = events_values.replace(False, "Non").replace(True, "Oui")
#         events_values = events_values.replace("False", "Non").values[0]
#         bilan_data = dict(events_labels=events_labels, events_values=events_values)
#         source = ColumnDataSource(bilan_data)
#         columns = [
#             TableColumn(field="events_labels", title="Type"),
#             TableColumn(field="events_values", title="Valeur"),
#         ]
#         event_plot = DataTable(source=source, columns=columns, width=400, height=280)
#
#         grid = layout([
#             [event_plot, bilan_plot, match_plot],
#         ], sizing_mode="scale_width")
#
#         script, div = components(grid)
#         context['script'] =script
#         context['div'] = div
#         return context
#
#     def post(self, request, *args, **kwargs):
#         context = self.get_context_data(**kwargs)
#         if any('answer__' in x for x in request.POST):
#             norm_str = [key for key in request.POST if 'answer__' in key][0]
#             norm_str = norm_str.split('__')[1]
#             norm = eval(norm_str)
#             contest_value = ContestValue.objects.get(id=request.session['contest_db_id'])
#             contestation = contestvalue_db_obj_to_internal_class(contest_value, 'y_true')
#             file = context['decision_data']
#             rbm, rbm_neg = pro_and_cons_rules(file, contestation, norm(), DATA,
#                                               features=['ttlgp', 'age_r', 'sex_r', 'abo_r', 'taille_r', 'poids_r',
#                                                         'urgence', 'da', 'drg', 'cec', 'siav', 'cat', 'bnp', 'probnp',
#                                                         'dialyse', 'creat', 'bili'])
#             norm_values = request.session['norm_values']
#             norm_idx = np.where([norm['name']==norm_str for norm in norm_values])[0][0]
#             if rbm:
#                 norm_values[norm_idx]['rbm_predicate'] = ' ET <br>'.join([''.join(r) for r in rbm[0]])
#                 norm_values[norm_idx]['rbm_ttest'] = rbm[2]
#             if rbm_neg:
#                 norm_values[norm_idx]['rbm_neg_predicate'] = ' ET <br>'.join([''.join(r) for r in rbm_neg[0]])
#                 norm_values[norm_idx]['rbm_neg_ttest'] = rbm_neg[2]
#             context["norm_values"] = norm_values
#             context["predicates"] = request.session['predicates']
#             context["target"] = request.session['target']
#             return render(request, 'justifications.html', context)
#
#         form = ContestValueForm(request.POST, file=context['decision_data'])
#         if form.is_valid():
#             contest_value = form.save(commit=False)
#             db_obj = File(**context['decision_data'][[f.column for f in File._meta.fields if not f.auto_created]])
#             db_obj.save()
#             contest_value.file = db_obj
#             contest_value.save()
#             request.session['contest_db_id'] = contest_value.pk
#             cv = contestvalue_db_obj_to_internal_class(contest_value, 'y_true')
#             request.session['contestation'] = repr(contest_value)
#             context["predicates"] = (' ET '.join([x[0] + operator_to_symbol(x[1]) + str(x[2]) for x in cv.predicate_list]))
#             request.session['predicates'] = context["predicates"]
#             t = cv.target_predicate
#             context["target"] = context['output_name'] + ' ' +  operator_to_symbol(t[1]) + ' ' + str(t[2])
#             request.session['target'] = context["target"]
#             all_norms = [DeathRiskNorm(), RarityNorm(), DistanceNorm(), UnderageNorm(), YouthNorm(), WaitNorm()]
#
#             contest_dataset = justification_dataset(cv, DATA, 'undetermined')
#             norm_values = []
#             for norm in all_norms:
#                 norm_values.append(
#                     (str(norm),
#                      norm.eval(contest_dataset),
#                      norm.evidence(cv, DATA, contest_dataset),
#                      norm.description)
#                 )
#             # norm_values = sorted(norm_values, key=lambda val: - abs(val[2][1][0]))
#             norm_values = [{'name': norm[0],
#                             'value': norm[1],
#                             'avg': norm[1] - norm[2][0][0],
#                             'size': norm[2][0][1],
#                             'pvalue': norm[2][1][1],
#                             'ttest': norm[2][1][0],
#                             'description': norm[3]} for norm in norm_values]
#             context['norm_values'] = norm_values
#             print(norm_values)
#             request.session['norm_values'] = norm_values
#         else:
#             context['form'] = form
#             return render(request, 'challenge.html', context)
#
#         return render(request, 'justifications.html', context)
