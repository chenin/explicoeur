from django.db import models
from django.contrib.contenttypes.fields import GenericRelation

from mysite.settings import *


CHOICE_SAME = (('True', 'True'), ('False', 'False'))
CHOICE_SEX_R = (('H', 'H'), ('F', 'F'))
CHOICE_ABO_R = (('O', 'O'), ('B', 'B'), ('A', 'A'), ('AB', 'AB'))
CHOICE_URGENCE = (('False', 'False'), ('XPCP2', 'XPCP2'), ('XPCA', 'XPCA'), ('XPCP1', 'XPCP1'))
CHOICE_DRG = (('False', 'False'), ('O', 'O'))
CHOICE_SIAV = (('False', 'False'), ('G', 'G'), ('D', 'D'), ('BV', 'BV'))
CHOICE_CAT = (('False', 'False'), ('O', 'O'))
CHOICE_DIALYSE = (('False', 'False'), ('O', 'O'))
CHOICE_CEC = (('False', 'False'), ('True', 'True'))

CATE_OPERATOR = (
    ('', ''),
    ('est', 'est'),
)

INT_OPERATOR = (
    ('', ''),
    ('>=', '>='),
    ('<=', '<='),
)

INT_OPERATOR_REL = (
    ('', ''),
    ('>=', '>='),
    ('<=', '<='),
    ('=', '='),
)

CHOICE_HELP = (
    ('MLA_VALUES', 'MLA_VALUES'),
    ('MLA_RELATIVE', 'MLA_RELATIVE'),
    ('COUNTERFACT', 'COUNTERFACT'),
    ('K_NEIGHBORS', 'K_NEIGHBORS'),
    ('NONE', 'NONE'),
)


class File(models.Model):
    ttlgp = models.FloatField(default=100, blank=True, null=True)
    age_diff = models.FloatField(default=100, blank=True, null=True)
    age_r = models.FloatField(default=100, blank=True, null=True)
    same_sex = models.CharField(max_length=100, choices=CHOICE_SAME, default=CHOICE_SAME[0], blank=True, null=True)
    sex_r = models.CharField(max_length=100, choices=CHOICE_SEX_R, default=CHOICE_SEX_R[0], blank=True, null=True)
    same_abo = models.CharField(max_length=100, choices=CHOICE_SAME, default=CHOICE_ABO_R[0], blank=True, null=True)
    taille_diff = models.FloatField(default=160, blank=True, null=True)
    poids_diff = models.FloatField(default=60, blank=True, null=True)
    urgence = models.CharField(max_length=100, choices=CHOICE_URGENCE, default=CHOICE_URGENCE[0], blank=True, null=True)
    da = models.FloatField(default=100, blank=True, null=True)
    drg = models.CharField(max_length=100, choices=CHOICE_DRG, default=CHOICE_DRG[0], blank=True, null=True)
    cec = models.CharField(max_length=100, choices=CHOICE_CEC, default=CHOICE_CEC[0], blank=True, null=True)
    siav = models.CharField(max_length=100, choices=CHOICE_SIAV, default=CHOICE_SIAV[0], blank=True, null=True)
    cat = models.CharField(max_length=100, choices=CHOICE_CAT, default=CHOICE_CAT[0], blank=True, null=True)
    bnp = models.FloatField(default=100, blank=True, null=True)
    probnp = models.FloatField(default=100, blank=True, null=True)
    dialyse = models.CharField(max_length=100, choices=CHOICE_DIALYSE, default=CHOICE_DIALYSE[0], blank=True, null=True)
    creat = models.FloatField(default=100, blank=True, null=True)
    bili = models.FloatField(default=100, blank=True, null=True)
    y_true = models.FloatField(default=0, blank=True, null=True)

#
# class ContestValue(models.Model):
#     file = models.ForeignKey(File, on_delete=models.CASCADE)
#     ttlgp_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['ttlgp'], blank=True, null=True)
#     ttlgp_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     age_diff_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['age_diff'], blank=True, null=True)
#     age_diff_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     age_r_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['age_r'], blank=True, null=True)
#     age_r_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     same_sex_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['same_sex'], blank=True, null=True)
#     same_sex_value = models.CharField(max_length=100, choices=CHOICE_SAME, verbose_name='', blank=True, null=True)
#
#     sex_r_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['sex_r'], blank=True, null=True)
#     sex_r_value = models.CharField(max_length=100, choices=CHOICE_SEX_R, verbose_name='', blank=True, null=True)
#
#     same_abo_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['same_abo'], blank=True, null=True)
#     same_abo_value = models.CharField(max_length=100, choices=CHOICE_SAME, verbose_name='', blank=True, null=True)
#
#     taille_diff_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['taille_diff'], blank=True, null=True)
#     taille_diff_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     poids_diff_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['poids_diff'], blank=True, null=True)
#     poids_diff_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     urgence_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['urgence'], blank=True, null=True)
#     urgence_value = models.CharField(max_length=100, choices=CHOICE_URGENCE, verbose_name='', blank=True, null=True)
#
#     da_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['da'], blank=True, null=True)
#     da_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     drg_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['drg'], blank=True, null=True)
#     drg_value = models.CharField(max_length=100, choices=CHOICE_DRG, verbose_name='', blank=True, null=True)
#
#     cec_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['cec'], blank=True, null=True)
#     cec_value = models.CharField(max_length=100, choices=CHOICE_CEC, verbose_name='', blank=True, null=True)
#
#     siav_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['siav'], blank=True, null=True)
#     siav_value = models.CharField(max_length=100, choices=CHOICE_SIAV, verbose_name='', blank=True, null=True)
#
#     cat_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['cat'], blank=True, null=True)
#     cat_value = models.CharField(max_length=100, choices=CHOICE_CAT, verbose_name='', blank=True, null=True)
#
#     bnp_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['bnp'], blank=True, null=True)
#     bnp_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     probnp_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['probnp'], blank=True, null=True)
#     probnp_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     dialyse_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['dialyse'], blank=True, null=True)
#     dialyse_value = models.CharField(max_length=100, choices=CHOICE_DIALYSE, verbose_name='', blank=True, null=True)
#
#     creat_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['creat'], blank=True, null=True)
#     creat_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     bili_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['bili'], blank=True, null=True)
#     bili_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     y_true_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Score", blank=True, null=True)
#     y_true_value = models.FloatField(verbose_name='', blank=True, null=True)

#
# class ContestValue(models.Model):
#     file = models.ForeignKey(File, on_delete=models.CASCADE)
#     ttlgp_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['ttlgp'], blank=True, null=True)
#     ttlgp_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     age_r_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['age_r'], blank=True, null=True)
#     age_r_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     sex_r_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['sex_r'], blank=True, null=True)
#     sex_r_value = models.CharField(max_length=100, choices=CHOICE_SEX_R, verbose_name='', blank=True, null=True)
#
#     abo_r_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['abo_r'], blank=True, null=True)
#     abo_r_value = models.CharField(max_length=100, choices=CHOICE_ABO_R, verbose_name='', blank=True, null=True)
#
#     taille_r_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['taille_r'], blank=True, null=True)
#     taille_r_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     poids_r_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['poids_r'], blank=True, null=True)
#     poids_r_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     urgence_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['urgence'], blank=True, null=True)
#     urgence_value = models.CharField(max_length=100, choices=CHOICE_URGENCE, verbose_name='', blank=True, null=True)
#
#     da_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['da'], blank=True, null=True)
#     da_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     drg_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['drg'], blank=True, null=True)
#     drg_value = models.CharField(max_length=100, choices=CHOICE_DRG, verbose_name='', blank=True, null=True)
#
#     cec_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['cec'], blank=True, null=True)
#     cec_value = models.CharField(max_length=100, choices=CHOICE_CEC, verbose_name='', blank=True, null=True)
#
#     siav_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['siav'], blank=True, null=True)
#     siav_value = models.CharField(max_length=100, choices=CHOICE_SIAV, verbose_name='', blank=True, null=True)
#
#     cat_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['cat'], blank=True, null=True)
#     cat_value = models.CharField(max_length=100, choices=CHOICE_CAT, verbose_name='', blank=True, null=True)
#
#     bnp_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['bnp'], blank=True, null=True)
#     bnp_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     probnp_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['probnp'], blank=True, null=True)
#     probnp_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     dialyse_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name=VAR_NAME_DICT['dialyse'], blank=True, null=True)
#     dialyse_value = models.CharField(max_length=100, choices=CHOICE_DIALYSE, verbose_name='', blank=True, null=True)
#
#     creat_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['creat'], blank=True, null=True)
#     creat_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     bili_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name=VAR_NAME_DICT['bili'], blank=True, null=True)
#     bili_value = models.FloatField(verbose_name='', blank=True, null=True)
#
#     y_true_operator = models.CharField(max_length=100, choices=INT_OPERATOR, verbose_name="Score", blank=True, null=True)
#     y_true_value = models.FloatField(verbose_name='', blank=True, null=True)
#



# class ContestRelative(models.Model):
#     plaintiff_file = models.ForeignKey(File, on_delete=models.CASCADE, related_name='plaintiff_file')
#     relative_file = models.ForeignKey(File, on_delete=models.CASCADE, related_name='relative_file')
#     age_operator = models.CharField(max_length=100, choices=INT_OPERATOR_REL, verbose_name="Age", blank=True, null=True)
#     nombre_condamnations_anterieures_operator = models.CharField(max_length=100, choices=INT_OPERATOR_REL,
#                                                                  verbose_name="Nombre de delits anterieurs", blank=True,
#                                                                  null=True)
#     genre_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Genre", blank=True,
#                                       null=True)
#     origine_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Origine ethnique",
#                                         blank=True, null=True)
#     description_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Description",
#                                             blank=True, null=True)
#     type_operator = models.CharField(max_length=100, choices=CATE_OPERATOR, verbose_name="Type", blank=True, null=True)
#     target_plaintiff = models.BooleanField()
#     output_relative = models.BooleanField()
#     counter_example = GenericRelation(CounterExampleRelative, content_type_field='contest_content_type',
#                                       object_id_field='contest_object_id')
#     valid_proportion = GenericRelation(ValidProportion)
#     rbm = GenericRelation(RuleBasedModel)
#     rbm_negative = GenericRelation(RuleBasedModel)
#
#
# class Task(models.Model):
#     user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
#     sample = models.ForeignKey(File, on_delete=models.CASCADE, related_name="first")
#     help_type = models.CharField(max_length=100, choices=CHOICE_HELP, default=CHOICE_HELP[0])
#     true_answer = models.BooleanField()
#     user_answer = models.NullBooleanField(blank=True, null=True)
#     likert_confidence = models.PositiveSmallIntegerField(validators=[MaxValueValidator(5)], blank=True, null=True)
#     comment = models.CharField(max_length=3000, blank=True, null=True)
#
#
# class ReferenceFile(models.Model):
#     sample = models.OneToOneField(File, on_delete=models.CASCADE)
#     reference = models.ForeignKey(File, on_delete=models.CASCADE, related_name='reference')
