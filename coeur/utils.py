from justify.contestation import *
import operator
from mysite.settings import *


def event_to_one_column(data):
    replace_dict = {
        'drg': {'O': 'DRG', 'False': '', 'N': ''},
        'cec': {True: 'CEC', False: '', 'False': '', 'True': 'CEC'},
        'urgence': {'False': ''},
        'siav': {'BV': "SIAV-BV", 'G': 'SIAV-G', 'D': '', 'False': ''},
        'cat': {'N': '', 'False': '', 'O': 'CAT'},
        'dialyse': {'False': '', 'O': "Dialyse", '?': ''}
    }
    one_feat = data[replace_dict.keys()].replace(replace_dict)
    data['events'] = one_feat.apply(lambda x: ', '.join([item for item in x if item != '']), axis=1)
    del data['cec'], data['urgence'], data['siav'], data['cat'], data['dialyse'], data['drg']
    return data


def convert_operator(symbol):
    if symbol == 'est':
        return operator.eq
    elif symbol == '=':
        return operator.eq
    elif symbol == "n'est pas":
        return operator.ne
    elif symbol == '>=':
        return operator.ge
    elif symbol == '<=':
        return operator.le
    else:
        raise ValueError("Symbol " + symbol + " is not valid. ")


def contestvalue_db_obj_to_internal_class(contest_db_obj, target_feature):
    contest_data = {k: v for k, v in contest_db_obj.__dict__.items()
                    if ('_value' in k or 'operator' in k) and v is not None}
    predicate_list = []
    feature_set = {key.replace('_value', '').replace('_operator', '')
                   for key in contest_data.keys()}
    for feat in feature_set:
        if contest_data.get(feat + '_operator', None) is not None:
            op = convert_operator(contest_data[feat + '_operator'])
            value = contest_data[feat + '_value']
            if feat == target_feature:
                target_predicate = (feat, op, value)
            else:
                predicate_list.append((feat, op, value))
    return ContestVal(predicate_list, target_predicate)


def inject_form(request):
    from coeur.forms import OutputFeaturesForm
    form = OutputFeaturesForm()
    if request.session.get('output', False):
        output = request.session.get('output', False)
        features = request.session.get('features', False)
        var_dict = {'output': output}
        for form_field in form.fields.keys():
            if form_field!='output':
                var_dict[form_field] = (form_field in features)
        form.initial = var_dict
    return {'feat_output_form': form}


def mean_min_max_info(df_col):
    if len(df_col) > 0:
        return str(round(df_col.mean())) + '  (min: ' + str(round(df_col.min())) + ' -- max: ' + str(round(df_col.max())) + ')'
    else:
        return '////'


def convert_feature_name(feature_name):
    if feature_name in VAR_NAME_DICT.keys():
        return VAR_NAME_DICT[feature_name]
    if "__is__" in feature_name:
        feat, val = feature_name.split('__is__')
        try:
            # variable value must be converted before being displayed
            return VAR_NAME_DICT[feat] + ' est ' + CATE_VALUE_DICT[feat][val]
        except KeyError:
            return VAR_NAME_DICT[feat] + ' est ' + val
    else:
        return feature_name


def is_numeric(obj):
    attrs = ['__add__', '__sub__', '__mul__', '__truediv__', '__pow__']
    return all(hasattr(obj, attr) for attr in attrs)


def format_feature_value(feat, value):
    try:
        value = int(str(value))
    except ValueError:
        try:
            value = float(str(value))
        except ValueError:
            None
    if is_numeric(value):
        if type(value) == int:
            return str(value)
        else:
            return str(round(value, 2))
    else:
        if feat in CATE_VALUE_DICT.keys():
            return CATE_VALUE_DICT[feat][value]
        else:
            return value
