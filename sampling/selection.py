from settings import *
from sampling.sampling import *
sampling_conf = cfg['sampling']


def identity_sampling(base, model, actionable_features=[]):
    samples = base.population.copy()
    samples['output'] = model(samples)
    if len(actionable_features) > 0:
        return samples[actionable_features + ['output']]
    else:
        return samples


class IdentitySampling(SamplingMethod):

    def __init__(self, *args, **kwargs):
        super(IdentitySampling, self).__init__(*args, **kwargs)
        self.size = self.param.get('size', 500)
        self.convert_to_binary_class = self.param.get('convert_to_binary_class', False)

    def sample(self, base, scope, **kwargs):
        """
        Identity sampling : return the base.population
        """
        samples = base.population.copy()
        return samples


def select_closest(base, size=sampling_conf['select_closest']['size'],
                   convert_to_binary_classification=cfg['sampling']['convert_to_binary_classification'],
                   actionable_features=[]):
    similarities = base.pop_similarity_to_x(base.scope)
    if type(size) == float:
        n_closest = round(len(base.population) * size)
    elif type(size) == int:
        n_closest = size
    pop_copy = base.population.copy()
    pop_copy['simx'] = similarities
    pop_copy.sort_values('simx', ascending=True, inplace=True)
    samples = pop_copy.tail(n_closest).drop('simx', axis=1)
    samples['output'] = base.y_raw[samples.index]
    if convert_to_binary_classification:
        # for classification problem, compare with the class of x_e
        new_out = (samples['output']==base.model(base.scope)[0]).astype(int)
        samples.loc[:, 'output'] = new_out
    if len(actionable_features) > 0:
        return samples[actionable_features + ['output']]
    else:
        return samples
