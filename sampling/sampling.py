from abc import ABCMeta, abstractmethod
import copy
from shared.utils import *

class SamplingMethod():

    def __init__(self, base, sampling_param, actionable_features=[], coupled_var=[]):
        self.base = copy.deepcopy(base)
        # self.base.y_raw = self.base.y_raw[~np.any(pd.isnull(self.base.population[actionable_features]), axis=1)]
        self.param = sampling_param
        if len(actionable_features) > 0:
            self.features = actionable_features
        else:
            self.features = list(self.base.population.columns)
        self.original_features = copy.deepcopy(self.features)
        self.coupled_var = coupled_var
        self.untuplized_pop = self.base.population.copy()
        if len(self.coupled_var) > 0:
            for var_tuple in self.coupled_var:
                self.base.population = tuplize_columns(self.base.population, var_tuple)
                self.features = [x for x in self.features if x not in var_tuple]
                self.features.append('_'.join(var_tuple))
        self.base_y0 = copy.deepcopy(self.base)
        self.base_y0.population = self.base_y0.population[base.model(self.untuplized_pop) > 0][self.features]
        self.base_y500 = copy.deepcopy(self.base)
        self.base_y500.population = self.base_y500.population[base.model(self.untuplized_pop) > 500][self.features]

    @abstractmethod
    def sample(self, base, scope, **kwargs):
        raise NotImplementedError("Must override sample")

    def generate_samples(self, base, scope, output_model, **kwargs):
        if len(self.coupled_var) > 0:
            for var_tuple in self.coupled_var:
                scope = tuplize_columns(scope, var_tuple)
        samples = self.sample(base, scope, **kwargs)

        if len(self.coupled_var) > 0:
            for var_tuple in self.coupled_var:
                coupled_name = '_'.join(var_tuple)
                for it, var in enumerate(var_tuple):
                    samples.loc[:, var] = samples[coupled_name].apply(lambda x: x[it])
                    scope.loc[:, var] = scope[coupled_name].apply(lambda x: x[it])
                del samples[coupled_name]
                del scope[coupled_name]

        samples['output'] = output_model(samples)
        return samples[self.original_features + ['output']]

    def y0_conditioned_sampling(self, scope, output_model):
        samples = self.generate_samples(self.base_y0, scope, output_model)
        return samples

    def y500_conditioned_sampling(self, scope, output_model):
        samples = self.generate_samples(self.base_y500, scope, output_model)
        return samples
