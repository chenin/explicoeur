from sampling.sampling import *
from settings import *

add_n_cfg = cfg['sampling']['add_noise']
rplc_fcfv = cfg['sampling']['fix_col_fix_val']
one_sample_rpl = cfg['sampling']['one_sample_rpl']


def add_random_noise(base, sigma=add_n_cfg['sigma'], rdm_distribution=add_n_cfg['distribution'],
                     size=add_n_cfg['size'], positive_bool=add_n_cfg['positive_bool'],
                     convert_to_binary_class=cfg['sampling']['convert_to_binary_classification'],
                     actionable_features=[]):
    """
    Add gaussian noise to a single sample to be explain.
    Work only with numerical features
    :param x_e: the point to be explained
    :param sigma: standard deviation of added noise
    :param std_dict: dictionary of feature's std
    :param size: the number of samples
    :return: pd.DataFrame : containing the perturbed samples
    """
    if base.categorical:
        non_cate_features = [x for x in base.feature_names if x not in base.categorical_features]
        cate_samples = pd.DataFrame()
        uni_vals = {col: base.population[col].unique() for col in base.categorical_features}
        for col in base.categorical_features:
            p = .8
            p_complement = (1 - p) / (len(uni_vals[col]) - 1)
            bool_array = uni_vals[col]==base.scope[col].values[0]
            p_array = bool_array * p + ~bool_array * p_complement
            cate_samples.loc[:, col] = np.random.choice(uni_vals[col], size=size, p=p_array)
        cate_samples.index = list(base.scope.index) * size
        non_cate_scope = base.scope[non_cate_features]
        scale = np.array(list((base.std.values))) * sigma
        if rdm_distribution=='normal':
            print("AN", sigma)
            perturbed_data = np.random.normal(non_cate_scope.values,
                                              scale,
                                              size=(size, len(scale)))
        if rdm_distribution=='lognormal':
            perturbed_data = np.random.normal(non_cate_scope.values,
                                              scale,
                                              size=(size, len(scale)))
        samples = pd.DataFrame(data=perturbed_data,
                               index=list(base.scope.index) * size,
                               columns=non_cate_features)
        samples = pd.concat([samples, cate_samples], axis=1)
    else:
        if base.data_type == 'image':
            base.init_bpixel_representation(which='blur')

        scale = np.array(list((base.std.values))) * sigma
        if rdm_distribution=='normal':
            perturbed_data = np.random.normal(base.scope.values,
                                              scale,
                                              size=(size, len(scale)))
        if rdm_distribution=='lognormal':
            perturbed_data = np.random.normal(base.scope.values,
                                              scale,
                                              size=(size, len(scale)))
        if positive_bool:
            perturbed_data = abs(perturbed_data)
        samples = pd.DataFrame(data=perturbed_data,
                               index=list(base.scope.index) * size,
                               columns=base.feature_names)

    samples['output'] = base.model(samples)
    if convert_to_binary_class:
        # for classification problem, compare with the class of x_e
        new_out = (samples['output']==base.model(base.scope)[0]).astype(int)
        samples.loc[:, 'output'] = new_out
    if len(actionable_features) > 0:
        return samples[actionable_features + ['output']]
    else:
        return samples

class Permutation(SamplingMethod):

    def __init__(self, *args, **kwargs):
        super(Permutation, self).__init__(*args, **kwargs)
        self.size = self.param.get('size', 5000)
        self.n_col_distrib = self.param.get('n_col_distrib', one_sample_rpl['n_rpl_distrib'])
        self.normal_sigma = self.param.get('normal_sigma', one_sample_rpl['normal_sigma'])
        self.image_bool_x_array = self.param.get('image_bool_x_array', False)
        self.text_bool_array = self.param.get('text_bool_array', False)
        self.convert_to_binary_class = self.param.get('convert_to_binary_class', )

    def sample(self, base, scope, **kwargs):
        if base.data_type == 'image':
            base.init_bpixel_representation(which='inversion')
        if len(self.features) > 0:
            unprotected = self.features
        else:
            unprotected = base.population.columns
        protected = list(set(base.population.columns).difference(set(unprotected)))
        # create the list of variables to be replaced (list of sets)
        var_ordered = unprotected + protected
        M = np.array([var_ordered] * self.size)
        if self.n_col_distrib=='uniform':
            n_col_modified_count = simulate_uniform_draw_counts(self.size, len(unprotected))  # simulate uniform draw
        if self.n_col_distrib=='normal':
            n_col_modified_count = simulate_normal_draw_counts(self.size, len(unprotected), self.normal_sigma)
        for n_cols_modified, counts in n_col_modified_count.items():
            if counts > 0:
                # create a bool row with n_cols_modified True and the rest as False
                # False / True encodes the columns that remains in the list
                bool_m = [False] * (len(unprotected) - n_cols_modified) + [True] * n_cols_modified
                sel_array = np.array([bool_m] * counts)
                # suffle to simulate a uniform drawing
                bool_array_sel = shuffle_along_axis(sel_array, axis=1)
                # add "False" element for unprotected variables
                if len(protected)>0:
                    concat_array = [bool_array_sel] + [[[False] * len(protected)] * len(bool_array_sel)]
                    bool_array_sel = np.concatenate(concat_array, axis=1)
                try:
                    all_sel_index = np.concatenate([all_sel_index, bool_array_sel], axis=0)
                except:
                    all_sel_index = bool_array_sel
        col_array = [set(M[i, ~idx]) for (i, idx) in enumerate(all_sel_index)]
        sub_population = base.population.sample(self.size, replace=True)
        samples = replace_col_values(sub_population, scope, col_array)
        # output = base.model(samples)
        if self.convert_to_binary_class:
            None
            # for classification problem, compare with the class of x_e
            # output = (output==base.model(scope)[0]).astype(int)
        if self.text_bool_array:
            samples.loc[:] = (samples.values == scope.values)
        if self.image_bool_x_array:
            samples = (samples != scope.index.values[0])
        # samples.loc[:, 'output'] = output
        samples = samples.set_index(pd.Index([scope.index.values[0]] * self.size))
        return samples



class PartialDepSampling(SamplingMethod):

    def __init__(self, *args, **kwargs):
        super(PartialDepSampling, self).__init__(*args, **kwargs)
        self.size = self.param.get('size', 500)
        self.convert_to_binary_class = self.param.get('convert_to_binary_class', False)

    def sample(self, base, scope, col_val_dict=None):
        """
        Sampling appropriate for Partial Dependance Plot.
        Change every value of columns of scope to a specified value
        :param col_val_dict: dict: key: name of the column, value: value to use
        :return: dataframe: same samples as scope with some columns modified
        """
        new_val_df = base.scope.sample(1)  # get one sample to have the format of the dataframe
        for k, v in col_val_dict.items():  # use column value dictionnary
            new_val_df.loc[:, k] = v
        samples = base.population.copy()
        samples = samples.sample(self.size, replace=True)
        for k, v in col_val_dict.items():
            coupled_index = np.where([k in x for x in self.coupled_var])[0]
            if len(coupled_index) > 0:
                samples[k] = v
                samples = tuplize_columns(samples, self.coupled_var[coupled_index[0]])
            else:
                samples[k] = v
        return samples
