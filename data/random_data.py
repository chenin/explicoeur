import numpy as np
import pandas as pd
import datetime as dt


def random_data_from_data(data, size=100, id_columns=[]):
    """
    Generate a random dataset (multivariate gaussian noise) from the mean
    and covariate values of real data. All features should be numeric. If covariate
    cannot be computed because of missing values, it will be set to zero.
    :param data: pandas dataframe
    categorical_features: list of discrete variable
    :return: multivariate gaussian noise
    """

    data_copy = data.copy()
    # convert datetime columns to time stamp
    dt_columns = []
    for col, dtype in data.dtypes.iteritems():
        if dtype in ['datetime64[ns]', 'datetime64']:
            data_copy[col] = pd.to_datetime(data_copy[col])
            data_copy[col] = (data_copy[col] - dt.datetime(1970, 1, 1)).astype('timedelta64[D]')
            dt_columns.append(col)

    encoded_samples = pd.get_dummies(data_copy, dummy_na=True, prefix_sep='__is__')

    for col in [x for x in encoded_samples.columns if '__is__' in x]:
        if all(encoded_samples[col] == 0):
            del encoded_samples[col]

    ids = encoded_samples[id_columns]
    encoded_samples = encoded_samples.drop(columns=id_columns)

    features = list(encoded_samples.columns)
    cate_fname = [x for x in encoded_samples.columns if '__is__' in x]
    categorical_features = [x for x in data_copy.columns if x not in features and x not in id_columns]

    for item in encoded_samples.dtypes.iteritems():
        if item[1] == 'datetime64[ns]':
            encoded_samples.loc[item[0]] = encoded_samples[item[0]].astype(int) / 10 ** 9

    mean = encoded_samples.mean()
    covariates = encoded_samples.corr().fillna(0)
    std = encoded_samples.std()
    covariates = covariates.multiply(std.multiply(std.T.values))
    rdm_data = np.random.multivariate_normal(mean.values, covariates.values, size=size)
    generated_df = pd.DataFrame(columns=features, data=rdm_data)
    generated_df = pd.concat([generated_df, ids], axis=1)

    # for encoded_feat in cate_fname:
    #     # convert continuous variables (gaussian) to discrete var (encoded categorical)
    #     # using the proportion of 0 and 1 in the encoded data
    #     if 'ABO' in encoded_feat:
    #         import pdb; pdb.set_trace()
    #     threshold = np.quantile(generated_df[encoded_feat], 1 - mean[encoded_feat])
    #     generated_df.loc[:, encoded_feat] = (generated_df[encoded_feat] > threshold).astype(int)

    for feat in categorical_features:
        col_sel = [x for x in generated_df.columns if x.startswith(feat + '__is__')]
        threshold = {}
        for col in col_sel:
            threshold[col] = np.quantile(generated_df[col], 1 - mean[col])
        df = generated_df[col_sel] - threshold
        df = df.idxmax(axis=1).apply(lambda x: x.replace(feat + '__is___', '').replace(feat + '__is__', ''))
        for c in col_sel:
            del generated_df[c]
        generated_df[feat] = df.replace('nan', np.nan)

    for dt_col in dt_columns:
        generated_df.loc[:, dt_col] = dt.datetime(1970, 1, 1) + pd.to_timedelta(generated_df[dt_col], 'D')
    return generated_df
