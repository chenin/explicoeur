# -*- coding: utf-8 -*-
import numpy as np
import datetime as dt
from settings import *
import pandas as pd


def f_ln_bili_la(bili, delay_bili):
    """
    Fonction Bilirubine en Liste d’attente
    :param bili: float or None: valeur de la mesure
    :param delay_bili: datetime: date de la mesure
    :return: float: compris entre ln(5) et ln(230)
    """
    bool_array_no_values = (bili.isnull() | delay_bili.isnull() | (delay_bili > cfg['score']['delay_bili']))
    bool_array_no_values = bool_array_no_values.astype(float)
    bili = bili.astype(float)
    values = np.log(np.amin([np.ones(len(bili)) * 230, np.amax([np.ones(len(bili)) * 5, bili], axis=0)], axis=0))
    values_if_missing = np.ones(len(bili)) * np.log(5)
    return np.where(bool_array_no_values, values_if_missing, values)


def f_ln_dfg_la(dialyse, creat, delay_creat, age_r, sex_r):
    """
     Fonction Débit de Filtration Glomérulaire en Liste d’attente (méthode MDRD)
    :param dialyse: bool : présence d'une dialyse ?
    :param creat: float or None: valeur de la créatine ?
    :param delay_creat: datetime: date de la mesure de créatine
    :param age_r: int: age du receveur
    :param sex_r: 'F' ou autre chose: sexe du receveur
    :return: float: compris entre 0 et ln(150)
    """
    bool_array_dialyse = (dialyse=='O')
    values_dialyse = np.ones(len(creat)) * np.log(15)
    bool_array_no_creat = (creat.isnull() | delay_creat.isnull() | (delay_creat > cfg['score']['delay_la']))
    values_no_creat = np.ones(len(creat)) * np.log(150)
    f_dfg = 186.3 * (creat / 88.4)**(-1.154) * (age_r**(-0.203))
    f_dfg *= (1 - 0.258 * (sex_r=='F'))
    f_dfg = f_dfg.astype(float)
    f_dfg = np.log(np.amin([np.ones(len(creat)) * 150, np.amax([np.ones(len(creat)), f_dfg], axis=0)], axis=0))
    output = np.where(bool_array_no_creat, values_no_creat, f_dfg)
    output = np.where(bool_array_dialyse, values_dialyse, output)
    return output


def f_decile_pn(cec, cat, siav, bnp, probnp, delay_probnp, delay_bnp):
    """
    Fonction décile des peptides natriurétiques (BNP ou NT-ProBNP)
    :param cec: bool: lien avec assitance de courte durée
    :param cat: bool: ??
    :param siav: 'B' ou autre chose: ??
    :param bnp: int or None: Peptides natriurétiques
    :param probnp: int or None: Peptides natriurétiques (autre méthode de dosage ?)
    :param delay_probnp: datetime: date de la mesure de probnp
    :param delay_bnp: datetime: date de la mesure de bnp
    :return: int: entre 1 et 10
    """
    bool_array_assist = ((cec == 'True') | (cat == 'O') | (siav == 'BV'))
    assist_values = np.ones(len(cec)) * 10

    bool_array_probnp_missing = (probnp.isnull() | delay_probnp.isnull() | (delay_probnp > cfg['score']['delay_probnp']))
    probnp_values = (1 + (probnp >= 928) + (probnp >= 1478) + (probnp >= 2044) + (probnp >= 2661) + (probnp >= 3416) + (probnp >= 4406) + (probnp >= 5645) + (probnp >= 8000) + (probnp >= 11332)).values
    default_values = np.ones(len(cec))

    # bool_array_bnp_missing = (bnp.isnull() | delay_bnp.isnull() | (delay_bnp > cfg['score']['delay_bnp']))
    # bnp_values = (1 + (bnp >= 189) + (bnp >= 314) + (bnp >= 481) + (bnp >= 622) + (bnp >= 818) + (bnp >= 1074) + (bnp >= 1317) + (bnp >= 1702) + (bnp >= 2696)).values
    #
    # output = np.where(bool_array_bnp_missing, default_values, bnp_values)
    output = np.where(bool_array_probnp_missing, default_values, probnp_values)  # change default_values to output if uncomment
    output = np.where(bool_array_assist, assist_values, output)

    return output


def f_crs(patient_df):
    """
    Calcul de l'index de risque cardiaque (0:40). Il prend en compte le risque de deces
    en liste d'attente.
    :param patient_df: dictionary containing all the variables
    :return: float : index de risque cardiaque entre 0 et 40.
    """

    # étapes intermédiaires pour le calcul de la fonction de risque pré-greffe

    ascd = (patient_df['cec'] == 'True').astype(int)

    decil_pn_args = {
        key: patient_df[key]
            for key in ['cec', 'cat', 'siav', 'bnp', 'probnp', 'delay_probnp', 'delay_bnp']
    }
    decile_pn = f_decile_pn(**decil_pn_args)

    dfg_la_args = {
        key: patient_df[key]
            for key in ['dialyse', 'creat', 'delay_creat', 'age_r', 'sex_r']
    }
    ln_dfg_la = f_ln_dfg_la(**dfg_la_args)

    bili_la_args = {
        key: patient_df[key]
        for key in ['bili', 'delay_bili']
    }
    ln_bili_la = f_ln_bili_la(**bili_la_args)

    # calcul de la fonction de risque pré-greffe en liste d'attente
    risque_pre_grf = 1.301335 * ascd
    risque_pre_grf += 0.157691 * decile_pn
    risque_pre_grf -= 0.510058 * ln_dfg_la
    risque_pre_grf += 0.615711 * ln_bili_la
    return risque_pre_grf


def f_icar(patient_df):
    """
    Calcul de l'index de risque cardiaque (0:40). Il prend en compte le risque de deces
    en liste d'attente.
    :param patient_df: dictionary containing all the variables
    :return: float : index de risque cardiaque entre 0 et 40.
    """
    risque_pre_grf = f_crs(patient_df)

    # constante de l'ICAR
    c_icar = 0.157691 * 1 - 0.510058 * np.log(150) + 0.615711 * np.log(5)

    unbounded_icar = round((risque_pre_grf - c_icar)*10)
    bounded_icar = np.amin([np.ones(len(patient_df)) * 40, np.amax([np.zeros(len(patient_df)), unbounded_icar], axis=0)], axis=0)
    return bounded_icar


def f_icar_max(patient_df):
    """
    Renvoie le maximum entre l'ICAR à date et l'ICAR avant injection ou mise sous circulation
    """
    array_bool_urg = (patient_df['cec'] == 'True') | (patient_df['drg']=='O')
    icar_values = f_icar(patient_df)
    df_copy = patient_df.copy()
    columns_name_avi_dict = {'bnp_avi': 'bnp', 'probnp_avi': 'probnp', 'dialyse_avi': 'dialyse', 'creat_avi': 'creat', 'bili_avi': 'bili'}
    df_copy = df_copy.drop(columns=columns_name_avi_dict.values())
    df_copy = df_copy.rename(columns=columns_name_avi_dict)
    df_copy.loc[:, 'delay_bnp'] = 0
    df_copy.loc[:, 'delay_probnp'] = 0
    df_copy.loc[:, 'delay_bili'] = 0
    df_copy.loc[:, 'delay_creat'] = 0
    icar_avi_values = f_icar(df_copy)
    icar_avi_values = np.amax([icar_avi_values, icar_values], axis=0)
    return np.where(array_bool_urg, icar_avi_values, icar_values)


def f_icar_std(patient_df):
    # ICAR standardisé (entre 0 et 1051)
    icar_std = f_icar_max(patient_df) * 1000. / 40.
    icar_std = np.where(icar_std >= 775, icar_std + 51, icar_std)  # 775 = 31*1000/40 (cf doc)
    return icar_std


def f_comp_ascd(patient_df):
    """
    Diminution des points après la mise en place d'une CEC temporaire
    """
    array_bool_threshold_1 = (patient_df['delay_cec'] < cfg['score']['threshold_cec_1'])
    array_bool_threshold_2 = (patient_df['delay_cec'] < cfg['score']['threshold_cec_2'])
    values_all_points = np.ones(len(patient_df))
    values_no_point = np.zeros(len(patient_df))
    values_in_between = np.ones(len(patient_df)) - cfg['score']['decote_cec'] * (patient_df['delay_cec'] + 1 - cfg['score']['threshold_cec_1'])

    output = np.where(array_bool_threshold_2, values_in_between, values_no_point)
    output = np.where(array_bool_threshold_1, values_all_points, output)
    output = np.where(patient_df['cec'] == 'True', output, values_all_points)
    return output


def f_ccb(patient_df):
    """
    Le score cardiaque composite brut.
    :param icar: index de risque cardiaque
    :param ad_std: Composante Adulte Standard
    :param ad_xpr: Composante Expert Adulte(XPCA)
    :param ped_std: Composante Pédiatrique Standard
    :param ped_xpr: Composante Expert Pédiatrique(XPCP)
    :return:
    """
    # composante adulte standard
    icar_std = f_icar_std(patient_df)
    array_bool_siavg = (patient_df['assist_bonus'] == 'True') & (patient_df['siav'] == 'G') & (icar_std < 775)
    values_siavg = 775
    icar_std = np.where(array_bool_siavg, values_siavg, icar_std)

    # penalise les longues CEC
    # désactivé pour les explications (on suppose que la variable CEC donne accès à des points)
    # icar_std *= f_comp_ascd(patient_df)

    # composante pédiatrique standard
    array_bool_ped_std = ((patient_df['age_r'] < 18) & (patient_df['urgence'] == 'False'))
    values_ped_std = np.ones(len(patient_df)) * 775
    values_ped_std += 50. * np.amax([
        np.zeros(len(patient_df)), np.amin([np.ones(len(patient_df)), patient_df['da'] / 24.], axis=0)
    ], axis=0)
    icar_std = np.where(array_bool_ped_std, values_ped_std, icar_std)

    # composante expert adulte
    array_bool_adlt_kxprt = ((patient_df['age_r'] >= 18) & (~(patient_df['urgence'] == 'False')) & (patient_df['daurg'] >= 0))
    values_adlt_kxprt = np.amax([icar_std, np.ones(len(patient_df)) * 900], axis=0)

    array_bool_adlt_kxprt_dla = ((patient_df['age_r'] >= 18) & (~(patient_df['urgence'] == 'False')) & (patient_df['xpc'] > 0))
    values = np.amin([np.ones(len(patient_df)), patient_df['daurg'] / patient_df['xpc']], axis=0)
    values = 900 * np.amax([np.zeros(len(patient_df)), values], axis=0)
    values_adlt_kxprt_dla = np.amax([icar_std, values], axis=0)

    icar_std = np.where(array_bool_adlt_kxprt, values_adlt_kxprt, icar_std)
    icar_std = np.where(array_bool_adlt_kxprt_dla, values_adlt_kxprt_dla, icar_std)

    # composante expert enfant
    array_bool_ped_kxprt_1 = ((patient_df['age_r'] < 18) & (~(patient_df['urgence'] == 'False')) & (patient_df['daurg'] >= 0))
    values_ped_kxprt_1 = np.amax([icar_std, np.ones(len(patient_df)) * 1102], axis=0)

    array_bool_ped_kxprt_2 = ((patient_df['age_r'] < 18) & (patient_df['urgence']=='XPCP2') & (patient_df['daurg'] >= 0))
    values_ped_kxprt_2 = np.amax([icar_std, np.ones(len(patient_df)) * 1051], axis=0)

    icar_std = np.where(array_bool_ped_kxprt_1, values_ped_kxprt_1, icar_std)
    icar_std = np.where(array_bool_ped_kxprt_2, values_ped_kxprt_2, icar_std)

    array_bool_ped_kxprt = ((patient_df['age_r'] < 18) & (~(patient_df['urgence'] == 'False')) & (patient_df['daurg'] >= 0))
    icar_std += 50 * np.nanmax([
        np.zeros(len(patient_df)), np.amin(
            [np.ones(len(patient_df)), patient_df['daurg'] / 30. / 24.],
            axis=0)],
        axis=0) * array_bool_ped_kxprt
    return icar_std


def f_diff_age(age_r, age_d):
    d_age = age_r - age_d
    array_bool = (d_age < 0)
    values_neg = (d_age + 65.) / 25.
    values_pos = 1 - (d_age - 15.) / 25.
    values = np.where(array_bool, values_neg, values_pos)

    array_bool_adult = (age_r >= 18)
    adult_values = np.amin([np.ones(len(age_r)), np.amax([np.zeros(len(age_r)), values], axis=0)], axis=0)
    child_values = np.ones(len(age_r))
    return np.where(array_bool_adult, adult_values, child_values)


def f_s_corpo(taille, poids):
    """
    Formule pour calculer la surface corporelle d'un patient
    """
    return 0.007184 * taille**(0.725) * poids**(0.425)


def f3_sc(age_r, sc_r, sc_d, sex_d, poids_d):
    """
    Fonction de prise en compte des différences de surface corporelle entre donneur et receveur
    """
    val = (
        ((age_r >= 18) & ((.8 * sc_r < sc_d) | ((sex_d=='H') & (poids_d >= 70))))
        | (age_r < 18) & ((.8 * sc_r < sc_d) & (2. * sc_r > sc_d)) | ((sex_d=='H') & (poids_d >= 70))
    ).astype(int)
    return val


def f_risk_post_gref(patient_df):
    """
    Calcul du risque post greffe
    """
    age_r_bool = (patient_df['age_r'] > 50).astype(int)

    mal_bool = patient_df['mal_set'].apply(eval).apply(lambda x: bool(x.intersection(set([162, 163, 815])))) # return True is at least one mal is in the list else False

    bili = patient_df['bili']
    delay_bili = patient_df['delay_bili']
    bool_array_no_values = (delay_bili.isnull() | bili.isnull() | (delay_bili > cfg['score']['delay_bili']))
    values = np.log(np.amin([np.ones(len(bili)) * 230, np.amax([np.ones(len(bili)) * 5, bili], axis=0)], axis=0))
    values_if_missing = np.ones(len(bili)) * np.log(230)
    ln_bili_grf = np.where(bool_array_no_values, values_if_missing, values)

    dialyse = patient_df['dialyse']
    creat = patient_df['creat']
    delay_creat = patient_df['delay_creat']
    age_r = patient_df['age_r']
    sex_r = patient_df['sex_r']

    bool_array_dialyse = (dialyse=='O')
    values_dialyse = np.ones(len(creat)) * np.log(15)
    bool_array_no_creat = (delay_creat.isnull() | creat.isnull() | (delay_creat > cfg['score']['delay_la']))
    values_no_creat = np.ones(len(creat)) * np.log(1)
    f_dfg = 186.3 * (creat / 88.4)**(-1.154) * (age_r**(-0.203))
    f_dfg *= (1 - 0.258 * (sex_r=='F'))
    f_dfg = np.log(np.amin([np.ones(len(creat)) * 150, np.amax([np.ones(len(creat)), f_dfg], axis=0)], axis=0))
    ln_dfg_grf = np.where(bool_array_no_creat, values_no_creat, f_dfg)
    ln_dfg_grf = np.where(bool_array_dialyse, values_dialyse, ln_dfg_grf)

    sex_bool = ((patient_df['sex_d']=='F') & (patient_df['sex_r']=='H')).astype(int)

    age_d_bool = (patient_df['age_d'] > 55).astype(int)

    risk_post_gref = 0.50608 * age_r_bool
    risk_post_gref += 0.50754 * mal_bool
    risk_post_gref += 0.40268 * ln_bili_grf
    risk_post_gref -= 0.54443 * ln_dfg_grf
    risk_post_gref += 0.36262 * sex_bool
    risk_post_gref += 0.41714 * age_d_bool
    return risk_post_gref


def matching_coefficient(patient_df):
    # appariement age
    coef_age_diff = f_diff_age(patient_df['age_r'], patient_df['age_d'])

    # appariement groupe sanguin
    coef_abo = (
        (patient_df['abo_r']==patient_df['abo_d'])
        | ((patient_df['abo_d']=='A') & (patient_df['abo_r']=='AB'))
        | ((patient_df['abo_d']=='O') & (patient_df['abo_r']=='B'))
    ).astype(int) + ((patient_df['abo_d']=='B') & (patient_df['abo_r']=='AB')).astype(int) * 0.1

    # compatibilité morphologique
    s_corpo_r = f_s_corpo(patient_df['taille_r'], patient_df['poids_r'])
    s_corpo_d = f_s_corpo(patient_df['taille_d'], patient_df['poids_d'])
    coef_s_corpo = f3_sc(patient_df['age_r'], s_corpo_r, s_corpo_d, patient_df['sex_d'], patient_df['poids_d'])
    return coef_age_diff * coef_abo * coef_s_corpo


def f_ccp(patient_df):
    """
    Calcul du Score Composite Pondéré. Application au Score
    Cardiaque Composite Brut (Score CCB) d’un ensemble de filtres et de fonctions
    d’appariement donneur receveur. Ces filtres s’appliquent lors de la proposition
    du greffon cardiaque.
    :param ccb: score cardiaque composite brut
    :param age_diff: différence d’âge entre le receveur et le donneur
    :param abo_comp: compatibilité ABO entre receveur et donneur
    :param morpho_comp: compatibilité morphologique entre donneur et receveur
    :param filtre_efficicaite: iltre d’efficacité en terme de résultats attendus de la greffe cardiaque
    :return:
    """
    # valeur du score composite brut du receveur
    ccb = f_ccb(patient_df)

    matching_coef = matching_coefficient(patient_df)

    # filtre efficacité en terme de résultats attendus de la greffe
    risk_post_grf = f_risk_post_gref(patient_df)
    coef_surv_post_grf = 0.6785748856**(np.exp(risk_post_grf))

    f4_surv_post_grf = ((coef_surv_post_grf > 0.5) | (patient_df['age_r'] < 18)).astype(int)

    return ccb * matching_coef * f4_surv_post_grf


def gravity_model(patient_df):
    return 1. / np.exp(0.0000002 * patient_df['ttlgp']**2.9)


def f_snacg(patient_df):
    """
    Calcul du score national d'attribution des greffons cardiaques.
    :param ccp: score composite pondéré
    :param ttlgp: durée du trajet entre les lieux de prélèvement et de greffe
    :return:
    """
    pat_df = patient_df.copy()
    ccp = f_ccp(pat_df)
    mg = gravity_model(pat_df)
    return ccp * mg


def f_rank(df):
    df_copy = df.copy()
    df_copy['snacg'] = f_snacg(df_copy)
    df_copy['ccp'] = f_snacg(df_copy)
    df_copy['ccb'] = f_snacg(df_copy)
    return df_copy.sort_values(['snacg', 'ccp', 'ccb', 'NUMDON_ANON'], ascending=False).groupby('NUMDON_ANON').cumcount() + 1
