from data.db_type_definition import *
from data.saved_models.compute_score_coeur_vectorised import *
import os
from mysite.settings import *

class ABMBase(TabularBase):
    def __init__(self):
        self.categorical_features = [
            "sex_r", "abo_r", "urgence", "drg", "cec", "siav", "cat", "dialyse", "sex_d", "abo_d", "mal_set",
            "EQUIPGRF", "ETATLA", 'dialyse_avi', "assist_bonus"
        ]
        self.feature_names = [
            'EQUIPGRF', 'SITPREL', 'ttlgp', 'NATT_ANON', 'age_r', 'sex_r', 'abo_r',
            'taille_r', 'poids_r', 'urgence', 'daurg', 'DINSCMED_ANON', 'ETATLA',
            'DETATLA_ANON', 'xpc', 'KXPC', 'TYPBIL', 'da', 'MODBIL', 'drg',
            'DDRG2_ANON', 'cec', 'delay_cec', 'siav', 'DAV2_ANON', 'cat',
            # 'bnp', 'dt_bnp',
            'probnp', 'dt_probnp', 'dialyse', 'creat', 'dt_creat', 'bili',
            'dt_bili', 'bnp_avi', 'probnp_avi', 'dialyse_avi', 'creat_avi',
            'bili_avi', 'CB_ID', 'NEXT_DINS', 'NUMDON_ANON', 'sex_d', 'age_d',
            'abo_d', 'taille_d', 'poids_d', 'TYPDON', 'TYPDONL', 'REGPREL', 'YPRL',
            'DCEC', 'DURG', 'DATB_ANON', 'DATPREL_ANON', 'mal_set', 'delay_bnp',
            'delay_probnp', 'delay_creat', 'delay_bili', "assist_bonus"
        ]

        super(ABMBase, self).__init__()
        self.one_hot_encoder = self.load_one_hot_encoder()
        self.regression = True
        self.classification = False
        self.features_recipient = [
            'EQUIPGRF', 'NATT_ANON', 'age_r', 'sex_r', 'abo_r',
            'taille_r', 'poids_r', 'urgence', 'daurg', 'DINSCMED_ANON', 'ETATLA',
            'DETATLA_ANON', 'xpc', 'KXPC', 'TYPBIL', 'da', 'MODBIL', 'drg',
            'DDRG2_ANON', 'cec', 'delay_cec', 'siav', 'DAV2_ANON', 'cat',
            'probnp', 'dt_probnp', 'dialyse', 'creat', 'dt_creat', 'bili',
            'dt_bili', 'bnp_avi', 'probnp_avi', 'dialyse_avi', 'creat_avi',
            'bili_avi', 'CB_ID', 'NEXT_DINS',
            'DCEC', 'DURG', 'DATB_ANON', 'DATPREL_ANON', 'mal_set', 'delay_bnp',
            'delay_probnp', 'delay_creat', 'delay_bili', 'ttlgp',
        ]
        self.features_giver = ['sex_d', 'age_d', 'abo_d', 'taille_d', "poids_d", "assist_bonus", ]

    def __str__(self):
        return "abm"

    def load_data(self):
        # data = pd.read_csv(os.path.join(BASE_DIR, "data/dataset/cbcdt.csv"))
        data = pd.read_csv(os.path.join(BASE_DIR, "data/dataset/cbcdt_fake.csv"))
        # data = data.head(5000)
        # data = data.sample(50000)
        # fill event missing values with the string "False"
        fillna_cols = ["urgence", "drg", "cec", "siav", "cat", "dialyse", "dialyse_avi"]
        data[fillna_cols] = data[fillna_cols].fillna(value='False')
        data.loc[:, 'assist_bonus'] = ((data['count'] % 5) == 0).astype(str)
        del data['count']
        data.loc[:, 'drg'] = data['drg'].replace("N", "False")
        data.loc[:, 'cec'] = data['cec'].astype(str)
        data.loc[:, 'cat'] = data['cat'].replace("N", "False")
        data.loc[:, 'dialyse'] = data['dialyse'].replace("?", "False")
        data.loc[:, 'dialyse_avi'] = data['dialyse_avi'].replace("?", "False").replace("N", "False")
        # convert bnp values in probnp values to decrease the number of missing values
        bnp = data['bnp']
        bnp = (1 + (bnp >= 189) + (bnp >= 314) + (bnp >= 481) + (bnp >= 622) + (bnp >= 818) + (bnp >= 1074) + (
                    bnp >= 1317) + (bnp >= 1702) + (bnp >= 2696))
        bnp[data['bnp'].isnull()] = np.nan
        bnp.replace({1: 928 / 2., 2: (1478 + 928) / 2., 3: (2044 + 1478) / 2.,
                4: (2661 + 2044) / 2., 5: (3416 + 2661) / 2., 6: (4406 + 3416) / 2.,
                7: (5645 + 4406) / 2., 8: (8000 + 5645) / 2., 9: (11332 + 8000) / 2.,
                10: 14664}, inplace=True)
        missing_probnp = data['probnp'].isnull().copy()
        data.loc[missing_probnp, 'probnp'] = bnp
        data.loc[missing_probnp, 'dt_probnp'] = data['dt_bnp']

        for dt_col in ["dt_probnp",  'dt_bnp', "dt_creat", "dt_bili", 'DINSCMED_ANON',
                       'DETATLA_ANON', 'DCEC', 'DURG', 'DDRG2_ANON', 'DAV2_ANON',
                       'NEXT_DINS', 'DATB_ANON', 'DATPREL_ANON', 'dt_today']:
            data[dt_col] = pd.to_datetime(data[dt_col])
        for dt_col in ["dt_probnp", "dt_creat", "dt_bili", "dt_bnp"]:
            new_col_name = dt_col.replace('dt', 'delay')
            data[new_col_name] = (data['dt_today'] - data[dt_col]).astype('timedelta64[D]')
        del data['dt_today']
        return data, None

    @staticmethod
    def load_model(which='snacg'):
        model = eval('f_' + which)
        return lambda x: model(x).values

    @staticmethod
    def display(samples):
        print(samples)
