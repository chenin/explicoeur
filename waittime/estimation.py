import pandas as pd

from shared.process_abm_db import *
from data.saved_models.compute_score_coeur_vectorised import *
from mysite.settings import *
import os

# def estimate_waiting_times(df, date):
#     THRESHOLD_RANKING = 3
#
#     donneur = pd.read_excel("/home/clement/Encfs/abm_data/sc_explanations/data/dataset/COHORTE_DON_ANONYM.xlsx")
#     dist = pd.read_excel("/home/clement/Encfs/abm_data/sc_explanations/data/dataset/PDIST.xlsx")
#     wait_list = df[(df['DATB_ANON'] < date) & (date < df['NEXT_DINS'])]
#     # keep only info about candidates registered at "date"
#     wait_list = wait_list[
#         ["NATT_ANON", "age_r", "sex_r", "abo_r", "taille_r", "poids_r", "EQUIPGRF",
#          "urgence", "daurg", "DINSCMED_ANON", "ETATLA", "DETATLA_ANON", "xpc", "KXPC", "TYPBIL",
#          "da", "MODBIL", "drg", "DDRG2_ANON", "cec", "delay_cec", "siav", "DAV2_ANON",
#          "cat", "bnp", "dt_bnp", "probnp", "dt_probnp", "dialyse", "creat", "dt_creat",
#          "bili", "dt_bili", "bnp_avi", "probnp_avi", "dialyse_avi", "creat_avi",
#          "bili_avi", "CB_ID", "NEXT_DINS", "DCEC", "DURG", "DATB_ANON", "mal_set",
#          "delay_bnp", "delay_probnp", "delay_bili", "delay_creat", ]
#          ].drop_duplicates(['NATT_ANON'])
#
#     donneur.loc[:, 'DATPREL_ANON'] = date
#     wait_list = merge_cbc_donneur(wait_list, donneur)
#     wait_list = merge_cbcd_dist(wait_list, dist)
#     wait_list = wait_list.rename(columns={"AVION": "ttlgp"})
#     wait_list['rank'] = f_rank(wait_list)
#     wait_list['mu'] = (wait_list['rank'] <= THRESHOLD_RANKING)
#     res = wait_list.groupby('NATT_ANON')['mu'].mean().to_frame()
#     for n_donneurs in [3, 7, 30, 90]:
#         res['proba_' + str(n_donneurs)] = 1 - (1 - res['mu'])**n_donneurs
#     return res


def estimate_wt_in_and_out_model(df, date, n_givers=30, n_experiments=30):
    donneur = pd.read_csv(os.path.join(BASE_DIR, "data/dataset/donneur_fake.csv"), parse_dates=["DATPREL_ANON"])
    dist = pd.read_excel(os.path.join(BASE_DIR, "data/dataset/PDIST.xlsx"))
    # keep only info about candidates registered at "date" (drop givers info)
    wait_list = df[(df['DATB_ANON'] < date) & (date < df['NEXT_DINS'])].drop_duplicates(['NATT_ANON'])
    new_candidates = df[~df.NATT_ANON.isin(wait_list.NATT_ANON)]
    # artificially change harvesting dates to avoid filters issues
    donneur.loc[:, 'DATPREL_ANON'] = date

    # select n_givers * n_experiments with replacement candidates (distinct NATT_ANON)
    added_cand = new_candidates.groupby('NATT_ANON', group_keys=False).apply(lambda x: x.sample(1))
    added_cand = added_cand.sample(n_givers * n_experiments, replace=True).drop_duplicates('NATT_ANON')
    added_cand.loc[:, 'DATB_ANON'] = date
    added_cand.loc[:, 'NEXT_DINS'] = date
    # compute all scores
    # TODO : pour les added_cand, il suffirait de calculer les scores de n_givers givers
    # TODO : et non pas les scores de n_givers * n_experiments donneurs comme c'est le cas ici.
    all_candidates = pd.concat([wait_list, added_cand], axis=0)
    all_candidates = all_candidates[
        ["NATT_ANON", "age_r", "sex_r", "abo_r", "taille_r", "poids_r", "EQUIPGRF",
         "urgence", "daurg", "DINSCMED_ANON", "ETATLA", "DETATLA_ANON", "xpc", "KXPC", "TYPBIL",
         "da", "MODBIL", "drg", "DDRG2_ANON", "cec", "delay_cec", "siav", "DAV2_ANON",
         "cat", "bnp", "dt_bnp", "probnp", "dt_probnp", "dialyse", "creat", "dt_creat",
         "bili", "dt_bili", "bnp_avi", "probnp_avi", "dialyse_avi", "creat_avi",
         "bili_avi", "CB_ID", "NEXT_DINS", "DCEC", "DURG", "DATB_ANON", "mal_set",
         "delay_bnp", "delay_probnp", "delay_bili", "delay_creat", "assist_bonus"]
    ]
    rdm_givers = donneur.sample(n_givers * n_experiments, replace=True).drop_duplicates('NUMDON_ANON')
    all_candidates = merge_cbc_donneur(all_candidates, rdm_givers)
    all_candidates = merge_cbcd_dist(all_candidates, dist)
    all_candidates = all_candidates.rename(columns={"AVION": "ttlgp"})
    all_candidates['score'] = f_snacg(all_candidates)
    # estimate one death risk per candidate based on CRS and 2017 mean death risk
    candidates_icar = all_candidates.groupby('NATT_ANON', group_keys=False).apply(lambda x: x.sample(1))
    # 0.0003550084333855479 = (N_décès + n aggrav 2017) / (N_patient 1 janv 2017 * 365) / np.mean(crs)
    # cette valeur de constante permet que le death_risk moyen soit égal au rapport DC/aggrav
    # sur 365 * nombre de patient
    candidates_icar['instant_risk'] = 0.0003550084333855479 * np.exp(f_crs(candidates_icar))
    candidates_icar['score'] = np.nan
    candidates_icar['NUMDON_ANON'] = -1
    all_candidates = pd.concat([all_candidates, candidates_icar], axis=0)

    all_candidates = all_candidates[['NATT_ANON', 'NUMDON_ANON', 'score', 'instant_risk']]
    # iterate over experiments
    results = []
    for _ in range(n_experiments):
        g_split = np.random.choice(all_candidates['NUMDON_ANON'].unique(), n_givers)
        a_split = np.random.choice(added_cand['NATT_ANON'].unique(), n_givers * 3)
        candidates = all_candidates[all_candidates['NUMDON_ANON'].isin(np.append(g_split, [-1]))].copy()
        exp_wait_list = candidates[candidates['NATT_ANON'].isin(wait_list.NATT_ANON.unique())]
        exp_added_cand = candidates[candidates['NATT_ANON'].isin(a_split)]

        # switch to matrix for efficiency
        # rows : (NATT_ANON) , columns : (score or icar, NUMDON)
        # one extra columns (corresponding to one value per patient) for the ICAR
        xtr = exp_added_cand.set_index(['NATT_ANON', 'NUMDON_ANON']).unstack('NUMDON_ANON').dropna(axis=1)
        wl = exp_wait_list.set_index(['NATT_ANON', 'NUMDON_ANON']).unstack('NUMDON_ANON').dropna(axis=1)
        exp_results = []
        for it, don in enumerate(wl['score'].columns):
            max_idx = wl['score'][don].idxmax()
            exp_results.append(max_idx)
            wl = wl.loc[wl.index != max_idx]
            wl = wl.append(xtr.head(1))
            xtr = xtr.drop(xtr.head(1).index)
            death_on_list = wl[(wl['instant_risk'][-1]) > np.random.rand(len(wl))]
            if len(death_on_list) > 0:
                wl = wl.loc[~wl.index.isin(death_on_list.index)]
                wl = wl.append(xtr.head(len(death_on_list)))
                xtr = xtr.drop(xtr.head(len(death_on_list)).index)
            results.append(exp_results)
    exp_ranks = {}
    for pat_natt in wait_list.NATT_ANON.values:
        exp_ranks[pat_natt] = [exp_res.index(pat_natt) if pat_natt in exp_res else np.inf for exp_res in results]
    thresholds = [3, 7, 14, 30]
    exp_proba = pd.DataFrame(columns=['proba_' + str(th) for th in thresholds])
    for pat_natt, ranks in exp_ranks.items():
        all_proba = {}
        for th in thresholds:
            proba = np.mean([r < th for r in ranks])
            all_proba['proba_' + str(th)] = proba
        exp_proba.loc[pat_natt] = all_proba
    exp_proba.index.name = 'NATT_ANON'
    return exp_proba
